﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MyLodge.proj1.Areas.Admin.Models;
using MyLodge.proj1.Areas.Admin.Models.CSM;//dir for CSM models
using MyLodge.proj1.Models.Aura;//dir for Aura models
using MyLodge.proj1.Utils;
using Newtonsoft.Json;

namespace MyLodge.proj1.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("Admin/[controller]")]
    public class CSMController : Controller
    {
        SysDiagDebug debug;
        public IActionResult Index()
        {
            return View();
            //not used yet
        }
        [HttpPost, Route("Dashboard")]
        [HttpGet]
        public IActionResult Dashboard()
        {
            //SysDiagDebug d = new SysDiagDebug();
            debug = new SysDiagDebug();
            AuraStatsView auraStats = new AuraStatsView();
            if (auraStats.Read() is null)
            {
                //debug.Out("AuraStats is null!");
                auraStats.Initialize();
                auraStats.Update();
            }
            AuraStats astats = new AuraStats();
            astats = auraStats.Read();
            auraStats = astats.SetAlternateCast<AuraStatsView>(auraStats); //profit
            AuraUserChat auc = new AuraUserChat();
            var logs = new AuraLogs();
            logs.DeleteOldLogs();
            HttpContext.Session.SetObject("f-logs", null);
            if (auraStats.getForwardedChats().Count > 0)
            {
                //have forwarded
                debug.Out("CSM>> Found (" + auraStats.getForwardedChats().Count + ") forwarded chats");
                //set number of pending inquiries
                ViewBag.fcount = auraStats.getForwardedChats().Count;
                //read first inquiry in bundle
                AuraLogs al = auraStats.getForwardedChats()[0];
                debug.Out("CSM>> Servicing the forwarded req with id = " + al.Chat_id);
                HttpContext.Session.SetObject("f-logs", auraStats.getForwardedChats()[0]); //instance2session
                ViewBag.Collection = auc.GetChat(auraStats.getForwardedChats()[0].Chat_id).chatHistory;//instance2viewbag
            }

            var chat = new List<string>();
            //if chat hist is not null
            if(HttpContext.Session.GetObject<AuraLogs>("f-logs") is null)
            {
                //debug.Out("Dashboard f-log is null!");
                chat.Add("r:Dashboard just reloaded!");
                ViewBag.Collection = chat;
            }
            /*
            var empData = HttpContext.Session.GetObject<employeeRegistration>("EIS");
            if(empData is null)
            {
                ViewBag.ename = "Emily Jackson";
                ViewBag.eid = "E103";
            }
            else
            {
                ViewBag.ename = empData.fname;
                ViewBag.eid = empData.eid;
            }*/
            //ViewBag.ar = auraStats.GetAllFAQ();
            return View("~/Areas/Admin/Views/CSM/Dashboard.cshtml", auraStats);
        }
        [HttpPost, Route("CSMChat")]
        [HttpGet]
        public IActionResult CSMChat(string msg)
        {
            debug = new SysDiagDebug();
            var alog = HttpContext.Session.GetObject<AuraLogs>("f-logs");
            //debug.Out("CSMChat>>AuraLog id = " + alog.Chat_id);
            if(!(alog is null))
            {
                //debug.Out("AuraLog Session detected!");
                if(msg == "End")
                {
                   // debug.Out("AuraLog Session Force Ended!");
                    alog.Forwarded = false;
                    HttpContext.Session.SetObject("f-logs", null);
                }
                else
                {
                    //debug.Out("AuraLog Session Ended!");
                    debug.Out("CSM>>Replied to chat with id = " + alog.Chat_id);
                    AuraUserChat auc = new AuraUserChat();
                    auc = auc.GetUserChat(alog.Chat_id);
                    auc.chatHistory.Add("r:" + msg);
                    auc.UpdateChat(alog.Chat_id);
                    alog.Forwarded = false;
                    alog.UpdateChatLogs();
                    //clear currently handled chat data for new data
                    HttpContext.Session.SetObject("f-logs", null);
                }
            }
            else
            {
                debug.Out("CSM>>alog is null!");
            }
            return RedirectToAction("Dashboard","CSM");
        }
        [HttpPost, Route("GetAuraLog")]
        public IActionResult GetAuraLog(int chatId, DateTime dt)
        {
            AuraChatView auraChat = new AuraChatView();
            auraChat = auraChat.GetChatForView(chatId);
            ViewBag.head = dt;
            return View("~/Areas/Admin/Views/CSM/LogView.cshtml",auraChat);
        }
        [HttpPost, Route("FAQ_data")]
        [HttpGet]
        public IActionResult FAQ_data()
        {
            //FAQ_model faq_s = new FAQ_model();
            FAQ_dataView faq = new FAQ_dataView();
            List<int> ids = faq.GetFAQIds();
            for (int i = 0; i < ids.Count; i++)
            {
                //debug.Out("faq id-> " + ids[i]);
            }
            return View("~/Areas/Admin/Views/CSM/FAQ_data.cshtml", faq);
        }
        [HttpPost, Route("EditFAQ_data")]
        public IActionResult EditFAQ_data(FAQ_model faq)
        {
            //debug.Out("EditFAQ Called using id = " + faq.Faq_id);
            Aura aura = new Aura();

            if(!(faq.Question is null))
            {
                //if faq is not empty
                if (faq.Faq_id == -1)
                {
                    //new record
                    faq.Q_score = 0;//reset score
                    faq.Keyword1 = "k1";
                    faq.Keyword2 = "k2";
                    faq.SaveNewUsingAura(aura.GetFaqKeywordsFromAura(faq.Question)); //will reset faq_id
                }
                else
                {
                    //update existing
                    faq.Q_score = 0;//reset score
                    faq.Keyword1 = "k1";
                    faq.Keyword2 = "k2";
                    faq.UpdateFAQUsingAura(aura.GetFaqKeywordsFromAura(faq.Question));
                }
            }
            //cannot pass models to view
            FAQ_dataView faq_v = new FAQ_dataView(); //hence instead a view
            //return View("~/Areas/Admin/Views/CSM/FAQ_data.cshtml", faq_v);
            return RedirectToAction("FAQ_data","CSM");//action,controller,values
        }
        [HttpPost, Route("DeleteFAQ_data")]
        public IActionResult DeleteFAQ_data(string FAQ_id)
        {
            FAQ_model faq = new FAQ_model();
            if(!(FAQ_id is null))
            {
                if (FAQ_id.Contains(','))
                {
                    string[] nums = FAQ_id.Split(",");
                    foreach (string number in nums)
                    {
                        faq.DeleteFAQ(number);
                    }
                }
                else
                {
                    faq.DeleteFAQ(FAQ_id);
                }
                
            }
            return RedirectToAction("FAQ_data","CSM");//action,controller,values
        }
        [HttpGet]
        [HttpPost, Route("ToggleAura")]
        public IActionResult ToggleAura(string turnOff, string AuraSwitch)
        {
            debug = new SysDiagDebug("CSM-Controller>>");
            //debug.Out("ToggleAura: " + turnOff);
            AuraStats astats = new AuraStats();
            astats = astats.Read();
            if (turnOff == "true") //turn aura off
            {
                if (astats.AuraSystemOn == true && AuraSwitch=="AuraSelfSwitch") //only if it's on
                {
                    debug.Out("Toggling Aura to OFF!");
                    astats.AuraSystemOn = false;
                    astats.UpdateAuraSwitch();
                }
                if (astats.AutoUpdateFAQ == true && AuraSwitch == "AuraFAQSwitch") //only if it's on
                {
                    debug.Out("Toggling Aura FAQ to OFF!");
                    astats.AutoUpdateFAQ = false;
                    astats.UpdateAuraFAQSwitch();
                }
            }
            else
            {
                //turn aura on
                if (astats.AuraSystemOn == false && AuraSwitch == "AuraSelfSwitch") //only if it's off
                {
                    debug.Out("Toggling Aura to ON!");
                    astats.AuraSystemOn = true;
                    astats.UpdateAuraSwitch();
                }
                if (astats.AutoUpdateFAQ == false && AuraSwitch == "AuraFAQSwitch") //only if it's off
                {
                    debug.Out("Toggling Aura FAQ to OFF!");
                    astats.AutoUpdateFAQ = true;
                    astats.UpdateAuraFAQSwitch();
                }
            }
            return RedirectToAction("Dashboard", "CSM");//reload to same page
        }
        [HttpGet]
        [HttpPost,Route("CSMSettingsPage")]
        public IActionResult CSMSettingsPage()
        {
            return View("~/Areas/Admin/Views/CSM/Settings.cshtml");
        }
        [HttpGet]
        [HttpPost, Route("GetLogView")]
        public IActionResult GetLogView(string chat_id)
        {
            int chatid = 0;
            try
            {
                chatid = Int32.Parse(chat_id);
            }
            catch (FormatException)
            {
                return RedirectToAction("Dashboard", "CSM");//reload to same page
            }
            ViewBag.chat_id = chat_id;
            AuraUserChat auc = new AuraUserChat();
            auc = auc.GetChat(chatid);
            ViewBag.Collection = auc.chatHistory;
            return View("~/Areas/Admin/Views/CSM/LogView.cshtml");
        }

        [HttpGet]
        [HttpPost, Route("GetTable2Excel")]
        public FileContentResult GetTable2Excel()
        {
            FAQ_model faq = new FAQ_model();
            List<FAQ_model> faqs = new List<FAQ_model>();
            faqs = faq.GetAllFAQ();
            string csv = String.Join(",", faqs.Select(x => x.ToString()).ToArray());
            return File(new System.Text.UTF8Encoding().GetBytes(csv), "text/csv", "FAQ_List.csv");
        }
    }//end of controller class
    public class AuraStatsView : AuraStats
    {
        public AuraStatsView():base("AuraStats")
        {
        }
        public ArrayList GetAllFAQ() //method?
        {
            SysDiagDebug d = new SysDiagDebug();
            FAQ_model faq = new FAQ_model();
            ArrayList ar = new ArrayList();
            foreach(var faqd in faq.GetAllFAQ())
            {
                //d.Out("faqd -> " + faqd.);
                ar.Add(faqd.Question);
                ar.Add(faqd.Answer);
                ar.Add(faqd.Keyword1);
                ar.Add(faqd.Keyword2);
            }
            return ar; //convert to arraylist
        }
    }
    public class FAQ_dataView : FAQ_model
    {
        public FAQ_dataView() : base("FAQ_model")
        {

        }
    }
    public class AuraChatView : AuraUserChat
    {
        public AuraChatView() : base()
        {

        }
        public AuraChatView GetChatForView(int id)
        {
            return (AuraChatView)base.GetChat(id);
        }
    }
    public static class SessionExtensions
    {
        public static void SetObject(this ISession session, string key, object value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }

        public static T GetObject<T>(this ISession session, string key)
        {
            var value = session.GetString(key);
            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }
    }
}
