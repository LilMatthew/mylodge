﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MyLodge.proj1.Areas.Admin.Models;
using Raven.Client.Documents;
using Raven.Client.Documents.Session;

namespace MyLodge.proj1.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("Admin/[controller]")]
    public class CusController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost, Route("Add")]
        public IActionResult Add()
        {
            return View("~/Areas/Admin/Views/Maintenance/addCustomerdata.cshtml");
        }

       

        [HttpPost]
        public IActionResult CusDetails()
        {
            return View("~/Areas/Admin/Views/Maintenance/addCustomerdata.cshtml");
        }

        [HttpPost]
        public IActionResult dash()
        {
            return View("~/Areas/Admin/Views/Maintenance/dashboard.cshtml");
        }


        [HttpPost, Route("Customer")]
        public IActionResult Customer(Customercheck hr)
        {
            hr.Save();
            CustomercheckView hrv = new CustomercheckView();
            // View("~/Views/Home/AdminCustomerMgt.cshtml", hrv);
            return View("~/Areas/Admin/Views/Maintenance/customerCheckin.cshtml", hrv);
            //return View("~/Areas/Admin/Views/Maintenance/addmaintenance.cshtml ", hrv); 
            //return View("~/Views/Maintenance/addmaintenance.cshtml",hrv);
        }


        [HttpPost, Route("ViewDashboard")]
        public IActionResult ViewDashboard()
        {
            CustomercheckView hrv = new CustomercheckView();
            return View("~/Areas/Admin/Views/Maintenance/customerCheckin.cshtml", hrv);
        }

        //search maintenance by id
        [HttpPost, Route("SearchMaint")]
        public IActionResult SearchMaint(String custId)  
        {

            Customercheck r = new Customercheck();
            CustomercheckView cv = new CustomercheckView();
            r = r.GetMintById(Int32.Parse(custId));

            cv.custId = r.custId;
            cv.custName = r.custName;
            cv.RoomNo = r.RoomNo;
            cv.phoneNo = r.phoneNo;
            cv.checkdate = r.checkdate;
            cv.checkOdate = r.checkOdate;

            if (r is null)
            {
                ViewBag.error = "Customer ID not found!";
                return View("~/Areas/Admin/Views/Maintenance/customerCheckin.cshtml");
            }
            else
            {

                return View("~/Areas/Admin/Views/Maintenance/customerCheckin.cshtml", cv);
            }
        }

        //navigate to update page
        [HttpPost, Route("EditMint")]
        public IActionResult EditMint(String id)
        {

            // HttpContext.Session.SetInt32("Maintenanceid", Int32.Parse(id));
            Customercheck r = new Customercheck();
            r = r.GetMintById(Int32.Parse(id));
            CustomercheckView hrv = new CustomercheckView();
            hrv = r.SetAlternateCast<CustomercheckView>(hrv, Int32.Parse(id));
            return View("~/Areas/Admin/Views/Maintenance/updateCustomer.cshtml", hrv);
        }


        // Upadate maintenance method
        [HttpPost, Route("UpdateMint")]
        [HttpGet]
        public IActionResult UpdateMint(Customercheck hr)
        {

            hr.UpdateCus();

            CustomercheckView st = new CustomercheckView();
            return View("~/Areas/Admin/Views/Maintenance/customerCheckin.cshtml", st);
        }

        //delete maintenance
        [HttpPost, Route("Delete")]
        [HttpGet]
        public IActionResult Delete(Customercheck s)
        {

            s.Delete(s.custId);
            CustomercheckView m = new CustomercheckView();
            return View("~/Areas/Admin/Views/Maintenance/customerCheckin.cshtml", m);

        }

        //generate report
        [HttpPost, Route("GetMintTableExcel")]
        public FileContentResult GetMintTableExcel()
        {
            Customercheck c = new Customercheck();
            List<Customercheck> cus = new List<Customercheck>();
            cus = c.GetAllCustomerdetails();
            string csv = String.Join(",", cus.Select(x => x.ToString()).ToArray());
            return File(new System.Text.UTF8Encoding().GetBytes(csv), "text/csv", "CustomrtCheckInList.csv");
        }



        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [HttpPost, Route("GetCusTable")]
        public IActionResult GetCusTable()
        {
            CustomercheckView cus = new CustomercheckView();
            return View("~/Areas/Admin/Views/Maintenance/addcustomerdata.cshtml", cus); //pass inherited class obj
        }
    }
    public class CustomercheckView : Customercheck
    {
        public CustomercheckView() : base("Customercheck")
        {

        }
    }


}
