﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MyLodge.proj1.Areas.Admin.Models;
using MyLodge.proj1.Models;
using MyLodge.proj1.Utils;
using Newtonsoft.Json;
using Raven.Client.Documents;
using Raven.Client.Documents.Session;

namespace MyLodge.proj1.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("Admin/[controller]")]
    //[Route("Admin/HomeController")]
    public class HomeController : Controller
    {
        SysDiagDebug d = new SysDiagDebug(false);//true turns off diagnostic msgs
        public IActionResult Index()
        {
            ViewBag.head1 = "Admin Home Controller";
            return View();
        }
        
        [HttpPost, Route("Login")]
        public IActionResult Login(employeeRegistration empl)
        {
            var emp = empl.GetEmployeeByNIC(empl.nic);
            d.Out("Login>> empl:nic=" + empl.nic + " password =" + empl.password);
            //d.Out("emp:nic=" + emp.nic + "password =" + emp.password + "eid= " + emp.eid);
            if (emp is null)
            {
                ViewBag.error = "Invalid NIC!";
                return View("~/Areas/Admin/Views/Home/Index.cshtml");//login
            }
            else
            {
                //NIC found
                //if(emp.PasswordIsMatching(emp.password))
                //if (emp.password == empl.password)
                if (emp.PasswordIsMatching(empl.password))
                {
                    //password correct
                    HttpContext.Session.SetObject("EIS", emp);
                    HttpContext.Session.SetObject("EIS_type", emp.feild_of_work);
                    HttpContext.Session.SetObject("EIS_rendertype", emp.GetMeaningfulFieldOfWork());
                    HttpContext.Session.SetObject("EIS_ename", emp.fname);
                    HttpContext.Session.SetObject("EIS_eid", "E"+(emp.eid+1));
                    d.Out("emp set=" + emp.eid);
                    //model class has CheckFieldOfWork() to prevent crashing into unimplemented files
                    //return View("~/Areas/Admin/Views/" + emp.feild_of_work + "/Dashboard.cshtml");
                    if (emp.feild_of_work == "GM")
                    {
                        //general manager
                        ViewBag.renderType = "General Manager";
                        return RedirectToAction("userpro", "Home");
                    }else if(emp.feild_of_work == "Mint")
                    {
                        //?
                        return RedirectToAction("SearchMaint", emp.feild_of_work);
                    }else if (emp.feild_of_work == "Stock")
                    {
                        //sdashboard
                        return RedirectToAction("sdashboard", "Stock");
                    }
                    else
                    {
                        return RedirectToAction("Dashboard", emp.feild_of_work);
                    }
                }
                else
                {
                    ViewBag.error = "Invalid Password!";
                    return View("~/Areas/Admin/Views/Home/Index.cshtml");//login
                }
            }
        }
        [HttpGet]
        [HttpPost, Route("Logout")]
        public IActionResult Logout()
        {
            HttpContext.Session.SetObject("EIS", null);
            HttpContext.Session.SetObject("EIS_ename", "E");
            HttpContext.Session.SetObject("EIS_rendertype", "Unknown Type");
            HttpContext.Session.SetObject("EIS_eid", "N/A");
            HttpContext.Session.SetObject("EIS_type", "N/A");
            return View("~/Areas/Admin/Views/Home/Index.cshtml");
        }
        [HttpGet]
        [HttpPost, Route("Signup")]
        public IActionResult Signup()
        {
            ViewBag.head1 = "Admin Home Controller";
            return View("~/Areas/Admin/Views/EIS/CreateAccount.cshtml");
        }


        [HttpPost, Route("Create_acc")]
        public IActionResult Create_acc(employeeRegistration hr)
        {
            d.Out("emp create account = " + hr.nic);
            hr.Save();
            HttpContext.Session.SetObject("EIS", hr);
            EmployeeView hrv = new EmployeeView();
            return View("~/Areas/Admin/Views/Home/Index.cshtml", hrv);
        }

        // [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        /*public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }*/

        //return leave form
        [HttpPost, Route("leave")]
        [HttpGet]
        public IActionResult leave()
        {
            ViewBag.head1 = "Admin Home Controller";
            var emp = HttpContext.Session.GetObject<employeeRegistration>("EIS");
            if (emp.feild_of_work == "CMS")
            {
                ViewBag.renderType = "Customer Support Manager";
                ViewBag.renderDashboardBtn = "<button id=\"Dashboard\" onclick=\"RequestDashboard()\">Dashboard</button>";
                ViewBag.renderSettingsBtn = "<button id=\"faq_data\" onclick=\"RequestFAQData()\">FAQ Data</button>";
                ViewBag.renderFAQBtn = "<button id=\"sys_Settings\" onclick=\"RequestSettings()\">System Settings</button>";
            }
            else
            {

            }
            return View("~/Areas/Admin/Views/EIS/LeaveForm.cshtml");
        }
        
        //after leave form submitted
        [HttpPost, Route("Apply_leave")]
        public IActionResult Apply_leave(EmployeeLeaveForm hrm)
        {
            hrm.Save();
            return View("~/Areas/Admin/Views/CSM/Dashboard.cshtml");
        }
        //return employee list
        [HttpPost, Route("userpro")]
        [HttpGet]
        public IActionResult userpro(employeeRegistration hr)
        {
            //hr.Save();
            EmployeeView hrv = new EmployeeView();
            ViewBag.head1 = "Admin Home Controller";
            return View("~/Areas/Admin/Views/EIS/EmployeeList.cshtml", hrv);
        }
        //delete currently logged on employee
        [HttpPost, Route("DeleteEmp")]
        [HttpGet]
        public IActionResult DeleteEmp()
        {
            //var cus = new employeeRegistration();
            //when nic parameter is not used, get session emp
            var cus = HttpContext.Session.GetObject<employeeRegistration>("EIS");
            //c.eid = cus.eid; //copy emp id from session emp
            d.Out("user eid=" + cus.eid);
            cus.Delete(cus.eid);
            return View("~/Areas/Admin/Views/Home/Index.cshtml");
        }
        //update currently logged on employee
        [HttpPost, Route("UpdateEmp")]
        [HttpGet]
        public IActionResult UpdateEmp(employeeRegistration c)
        {
            //var e = new employeeRegistration();
            var e = HttpContext.Session.GetObject<employeeRegistration>("EIS");
            c.eid = e.eid; //copy emp id from session emp
            d.Out("Update Emp ID = " + c.eid);
            c.UpdateEmp();
            c = c.Read(c.eid);
            HttpContext.Session.SetObject("EIS", c);
            HttpContext.Session.SetObject("EIS_ename", c.fname);
            HttpContext.Session.SetObject("EIS_eid", "E" + (c.eid + 1));
            HttpContext.Session.SetObject("EIS_rendertype", c.GetMeaningfulFieldOfWork());
            HttpContext.Session.SetObject("EIS_type", c.feild_of_work);
            return RedirectToAction("userAcc", "Home");
        }
        //return user profile
        [HttpPost, Route("userAcc")]
        [HttpGet]
        public IActionResult userAcc(employeeRegistration hr)
        {
            //hr.Save();
            EmployeeView hrv = new EmployeeView();
            ViewBag.head1 = "Admin Home Controller";
            var emp = new employeeRegistration();
            emp = HttpContext.Session.GetObject<employeeRegistration>("EIS");//from session
            hrv = emp.SetAlternateCast<EmployeeView>(hrv, emp.eid);
            if (emp.feild_of_work == "CMS")
            {
                ViewBag.renderType = "Customer Support Manager";
                ViewBag.renderDashboardBtn = "<button id=\"Dashboard\" onclick=\"RequestDashboard()\">Dashboard</button>";
                ViewBag.renderSettingsBtn = "<button id=\"faq_data\" onclick=\"RequestFAQData()\">FAQ Data</button>";
                ViewBag.renderFAQBtn = "<button id=\"sys_Settings\" onclick=\"RequestSettings()\">System Settings</button>";
            }
            if(emp.feild_of_work == "Stock")
            {
                return View("~/Areas/Admin/Views/Stock/stock_user_acc.cshtml", hrv);
            }
            return View("~/Areas/Admin/Views/EIS/UserAccount.cshtml", hrv);
        }
        //update in employee list
        [HttpPost, Route("updateEmpDetails")]
        [HttpGet]
        public IActionResult updateEmpDetails(employeeRegistration c)
        {
            c.UpdateEmpS();
            return RedirectToAction("userpro", "Home");
        }
        //delete from employee list
        [HttpPost, Route("deleteEmpDetails")]
        [HttpGet]
        public IActionResult deleteEmpDetails(employeeRegistration c)
        {
            //when nic parameter is not used, get session emp
            d.Out("emps eid=" + c.eid);
            c.Delete(c.eid);
            // return View("~/Areas/Admin/Views/Home/Index.cshtml");
            return RedirectToAction("userpro", "Home");
        }

        //return employee user account?
        [HttpPost, Route("GetEmployeeTablse")]
        public IActionResult GetEmployeeTables()
        {
            EmployeeView hrv = new EmployeeView();
            var emp = HttpContext.Session.GetObject<employeeRegistration>("EIS");
            if (emp.feild_of_work == "CMS")
            {
                ViewBag.renderType = "Customer Support Manager";
                ViewBag.renderDashboardBtn = "<button id=\"Dashboard\" onclick=\"RequestDashboard()\">Dashboard</button>";
                ViewBag.renderSettingsBtn = "<button id=\"faq_data\" onclick=\"RequestFAQData()\">FAQ Data</button>";
                ViewBag.renderFAQBtn = "<button id=\"sys_Settings\" onclick=\"RequestSettings()\">System Settings</button>";
            }
            else
            {

            }
            return View("~/Views/EIS/UserAccount.cshtml", hrv); //pass inherited class obj
        }

        //delete employee table
        [HttpPost, Route("getTabletoExecel")]
        [HttpGet]
        public IActionResult getTabletoExecel()
        {
            employeeRegistration emp = new employeeRegistration();
            List<employeeRegistration> emps = new List<employeeRegistration>();
            emps = emp.GetEmployeeDetails();
            string csv = String.Join(",", emps.Select(x => x.ToString()).ToArray());
            return File(new System.Text.UTF8Encoding().GetBytes(csv), "text/csv", "Employee_List.csv");
        }

    }
       

    public class EmployeeView : employeeRegistration
    {
        public EmployeeView() : base("employeeRegistration")
        {

        }

    }
}
