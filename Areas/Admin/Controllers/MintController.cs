﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MyLodge.proj1.Areas.Admin.Models;
using MyLodge.proj1.Utils;
using Newtonsoft.Json;
using Raven.Client.Documents;
using Raven.Client.Documents.Session;

namespace MyLodge.proj1.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("Admin/[controller]")]
    public class MintController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        [HttpPost, Route("Add")]
        public IActionResult Add()
        {
            return View("~/Areas/Admin/Views/Maintenance/addmaintenance.cshtml");
        }

        //add maintenance
        [HttpGet]
        [HttpPost, Route("Maintenance")]
        public IActionResult Maintenance(Maintenance hr)
        {
            //hr.Save();
            MaintenanceView hrv = new MaintenanceView();
            return View("~/Areas/Admin/Views/Maintenance/roomMaintenance.cshtml", hrv);
        }

        [HttpGet]
        [HttpPost, Route("ViewDashboard")]
        public IActionResult ViewDashboard()
        {
            MaintenanceView hrv = new MaintenanceView();
            return View("~/Areas/Admin/Views/Maintenance/roomMaintenance.cshtml", hrv);
        }

        //search maintenance by id
        [HttpGet]
        [HttpPost, Route("SearchMaint")]
        public IActionResult SearchMaint(String Maintenanceid)
        {
            SysDiagDebug d = new SysDiagDebug("Mint>>");
            //d.Out("id -> " + Maintenanceid);
            MaintenanceView cv = new MaintenanceView();
            if (!(Maintenanceid is null))
            {
                Maintenance r = new Maintenance();
                r = r.GetMintById(int.Parse(Maintenanceid));

                if (r is null)
                {
                    ViewBag.error = "Maintenance ID not found!";
                    return View("~/Areas/Admin/Views/Maintenance/roomMaintenance.cshtml", cv);
                }
                else
                {
                    cv.Maintenanceid = r.Maintenanceid;
                    cv.RoomNo = r.RoomNo;
                    cv.Toiletries = r.Toiletries;
                    cv.Items = r.Items;
                    cv.LcleanDate = r.LcleanDate;
                    cv.NcleanDate = r.NcleanDate;
                    return View("~/Areas/Admin/Views/Maintenance/roomMaintenance.cshtml", cv);
                }
            }
            return View("~/Areas/Admin/Views/Maintenance/roomMaintenance.cshtml", cv);
        }

        //navigate to update page
        [HttpPost, Route("EditMint")]
        public IActionResult EditMint(String id)
        {

           // HttpContext.Session.SetInt32("Maintenanceid", Int32.Parse(id));
            Maintenance r = new Maintenance();
            r = r.GetMintById(Int32.Parse(id));
            MaintenanceView hrv = new MaintenanceView();
            hrv = r.SetAlternateCast<MaintenanceView>(hrv, Int32.Parse(id));
            return View("~/Areas/Admin/Views/Maintenance/updatemaintenance.cshtml", hrv);
        }


        // Upadate maintenance method
        [HttpPost, Route("UpdateMint")]
        [HttpGet]
        public IActionResult UpdateMint(Maintenance hr)
        {

            hr.UpdateMaintenance();

            MaintenanceView st = new MaintenanceView();
            return View("~/Areas/Admin/Views/Maintenance/roomMaintenance.cshtml", st);
        }

        //delete maintenance
        [HttpPost, Route("Delete")]
        [HttpGet]
        public IActionResult Delete(Maintenance s)
        {

            s.Delete(s.Maintenanceid);
            MaintenanceView m = new MaintenanceView();
            return View("~/Areas/Admin/Views/Maintenance/roomMaintenance.cshtml", m);

        }

        //generate report
        [HttpGet]
        [HttpPost, Route("GetMintTableExcel")]
        public FileContentResult GetMintTableExcel()
        {
            Maintenance c = new Maintenance();
            List<Maintenance> cus = new List<Maintenance>();
            cus = c.GetAllMaintenancedetails();
            string csv = String.Join(",", cus.Select(x => x.ToString()).ToArray());
            return File(new System.Text.UTF8Encoding().GetBytes(csv), "text/csv", "MaintenanceList.csv");
        }
        [HttpGet]
        [HttpPost, Route("GetMaintenanceTable")]
        public IActionResult GetMaintenanceTable()
        {
            MaintenanceView cus = new MaintenanceView();
            return View("~/Areas/Admin/Views/Maintenance/addmaintenance.cshtml", cus); //pass inherited class obj
        }

    }
    public class MaintenanceView : Maintenance
    {
        public MaintenanceView() : base("Maintenance")
        {

        }
    }

}





