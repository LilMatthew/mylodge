﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
//using AspNetCore;
using Microsoft.AspNetCore.Mvc;
using MyLodge.proj1.Areas.Admin.Models;
using MyLodge.proj1.Utils;

namespace MyLodge.proj1.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("Admin/[controller]")]
    //[Route("Admin/HomeController")]
    public class StockController : Controller
    {
        SysDiagDebug d = new SysDiagDebug();
        public IActionResult Index()
        {

            ViewBag.head1 = "Admin Stock Controller";
            return View();
        }
        [HttpGet]
        [HttpPost, Route("sdashboard")]
        public IActionResult sdashboard()
        {
            //ViewBag.head1 = "Admin Home Controller";
            return View("~/Areas/Admin/Views/Stock/stockdashboard.cshtml");
        }

        [HttpPost, Route("Add_items")]
        [HttpGet]
        public IActionResult Add_items(stockdb hr)
        {
            SysDiagDebug d = new SysDiagDebug();
            d.Out("Add_items id" + hr.Item_Code);

            if (hr.Item_Name != null)
            {
                hr.Save();
            }
           
            stockdbView st = new stockdbView();
            return View("~/Areas/Admin/Views/Stock/additem.cshtml", st);
        }

        /*[HttpPost, Route("AddStock")]
        [HttpGet]
        public IActionResult AddStock(stockdb hr, string In, string Out)
        {
            hr.Save();
            stockdbView st = new stockdbView();
            return View("~/Areas/Admin/Views/Stock/addstock.cshtml", st);
        }
        */


        [HttpPost, Route("UpdateItem")]
        [HttpGet]
        public IActionResult UpdateItem(stockdb hr)
        {
           SysDiagDebug d = new SysDiagDebug();
            d.Out("Update id"+ hr.Item_Code +" amt ="+hr.Amount);
            hr.UpdateItem(hr.Item_Code);

            stockdbView st = new stockdbView();  
            return View("~/Areas/Admin/Views/Stock/additem.cshtml", st);
        }

        [HttpPost, Route("updateTot")]
        [HttpGet]
        public IActionResult updateTot(int id, int amt, string type)

        {
            SysDiagDebug d = new SysDiagDebug();
            d.Out("Update id" + id+ " amount =" +type + amt);

            stockdb u = new stockdb();
            if (type == "-")
            {
                amt *= -1;
            }
            u = u.Read(id);
            u.TotalQ += amt;
            u.UpdateTotal();
                       
            return RedirectToAction("Add_stock", "Stock");
        }

        //delete items
        [HttpPost, Route("Delete")]
         [HttpGet]
         public IActionResult Delete(stockdb s)
         {
           
             d.Out("Item_Code=" + s.Item_Code);
             s.Delete(s.Item_Code);
           
            return RedirectToAction("Add_items", "Stock");
         }

     

        /*[HttpPost]
        public IActionResult DeleteItem(stockdb s)
        {
            stockdbView st = new stockdbView();
            s.Delete(s.Item_Code);
            return View("~Areas/Admin/Views/Home/additem.cshtml");
        }
        */

        [HttpPost, Route("Dash_board")]
        [HttpGet]
        public IActionResult Dash_board(stockdb hr){
            //hr.Save();
            stockdbView hrv = new stockdbView();
            ViewBag.head1 = "Admin Home Controller";
            return View("~/Areas/Admin/Views/Stock/stockdashboard.cshtml", hrv);
        }

        [HttpPost, Route("Add_stock")]
        [HttpGet]
        public IActionResult Add_stock(stockdb hr){
            //hr.Save();
            stockdbView hrv = new stockdbView();
            ViewBag.head1 = "Admin Home Controller";
            return View("~/Areas/Admin/Views/Stock/addstock.cshtml", hrv);
        }

        [HttpPost, Route("Report")]
        [HttpGet]
        public IActionResult Report(stockdb hr){
            //hr.Save();
            stockdbView hrv = new stockdbView();
            ViewBag.head1 = "Admin Home Controller";
            return View("~/Areas/Admin/Views/Stock/ReportGeneration.cshtml", hrv);
        }

        [HttpPost, Route("Print")]
        [HttpGet]
        public IActionResult Print(stockdb hr)
        {
            //hr.Save();
            stockdbView hrv = new stockdbView();
            ViewBag.head1 = "Admin Home Controller";
            return View("~/Areas/Admin/Views/Stock/ReportGeneration.cshtml", hrv);
        }
        [HttpPost, Route("Logout")]
        [HttpGet]
        public IActionResult Logout(){
            //Logout
            return RedirectToAction("Logout", "Home");
        }
        [HttpPost, Route("User_Profile")]
        [HttpGet]
        public IActionResult User_Profile()
        {
            //Logout
            return RedirectToAction("userAcc", "Home");
        }
    }

    public class stockdbView : stockdb
    {
        public stockdbView() : base(){

        }
    }

   
}
