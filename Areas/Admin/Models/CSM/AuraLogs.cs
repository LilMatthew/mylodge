﻿using MyLodge.proj1.Areas.Admin.Controllers;
using MyLodge.proj1.Utils;
using Raven.Client.Exceptions;
using System;
using System.Collections.Generic;

namespace MyLodge.proj1.Areas.Admin.Models.CSM //dir set
{
    public class AuraLogs
    {
        [Newtonsoft.Json.JsonIgnore]
        private RavenDBConnector ravendb = RavenDBConnector.Instance();
        [Newtonsoft.Json.JsonIgnore]
        private static string chatType = "Chat";
        [Newtonsoft.Json.JsonIgnore]
        private SysDiagDebug debug = new SysDiagDebug("AuraLogs::");
        //public int Log_id { get; set; } //log_id = "AuraLogs/<type>/"
        public int Chat_id { get; set; }
        public DateTime Created { get; set; }
        public bool Has_spam { get; set; }
        public bool Forwarded { get; set; }
        public bool RegisteredCustomer { get; set; }
        public string Username { get; set; }

        public AuraLogs()
        {

        }
        public void CreateChatLog(bool has_spam, bool forwarded,bool regCustomer, string username, AuraUserChat chat)
        {
            Created = DateTime.Now;
            Chat_id = getNewPrimaryKey(chatType);
            //Chat_id = chat_id;
            Has_spam = has_spam;
            Forwarded = forwarded;
            RegisteredCustomer = regCustomer;
            Username = username;
            if (!regCustomer)
            {
                Username = "000";
            }
            chat.SaveChat(Chat_id);//save chat
            ravendb.SaveDocument(this.GetType().Name + "/" + chatType + "/" + Chat_id, this);//save log
        }
        public int getNewPrimaryKey(string type)
        {
            int counter = 0;
            try
            {
                AuraLogs logReader = new AuraLogs();
                bool forever = true;
                while (forever)
                {
                    logReader = ravendb.ReadDocument<AuraLogs>(this.GetType().Name + "/" + type+"/" + counter);
                    if (logReader is null)
                    {
                        break;
                    }
                    //Console.WriteLine("Logs::" + logReader.ToString());
                    counter++;
                }
            }
            catch (RavenException)
            {
                //return counter;
            }
            return counter;
        }
        public List<AuraLogs> ReadAllLogs()
        {
            return ravendb.QueryAll<AuraLogs>();
        }
        public List<AuraLogs> ReadAllLogsWithCondition(Func<AuraLogs,bool> condition)
        {
            return ravendb.QueryWhere<AuraLogs>(condition);
        }
        public void DeleteOldLogs()
        {
            //old means not today
            DateTime today = DateTime.Now;
            DateTime today_5mins_early = today.AddMinutes(-5);
            DateTime yesterday = today.AddDays(-1);
            List<AuraLogs> logBundle = new List<AuraLogs>();
            logBundle = ReadAllLogsWithCondition(x => x.Created <= today_5mins_early);
            debug.Out("Found " + logBundle.Count + " logs to be deleted!");
            foreach(AuraLogs log in logBundle)
            {
                debug.Out("Deleting>>" + log.ToString());
                log.DeleteChatLogs(log.Chat_id);
            }
        }
        public AuraLogs ReadChatLogs(int chat_id)
        {
            return ravendb.ReadDocument<AuraLogs>(this.GetType().Name + "/" + chatType + "/" + chat_id);
        }
        public void UpdateChatLogs()
        {
            string[] fields = { "Forwarded" };
            ravendb.UpdateDocument<AuraLogs>(this.GetType().Name + "/" + chatType + "/" + this.Chat_id, fields, this);
        }
        public void UpdateSelf(AuraUserChat auc)
        {
            string[] fields = { "Forwarded","Created"};
            this.Created = DateTime.Now;
            ravendb.UpdateDocument<AuraLogs>(this.GetType().Name + "/" + chatType + "/" + this.Chat_id, fields, this);
            auc.UpdateChat(Chat_id);
        }
        public void DeleteChatLogs(int chat_id)
        {
            ravendb.DeleteDocument(this.GetType().Name + "/" + chatType + "/" + chat_id);//delete log
            AuraUserChat chat = new AuraUserChat();
            chat.DeleteChat(chat_id);//delete chat
        }
        public override string ToString()
        {
            return "AuraLogs>> Log_id: " + this.GetType().Name + "/" + chatType + "/" + Chat_id + " Chat_id:" + Chat_id + " Has_spam:"+Has_spam+" Forwarded:"+Forwarded+" RegCustomer:"+RegisteredCustomer+" Username:"+Username+" Created:" + Created;
        }
    }
}
