﻿using MyLodge.proj1.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using MyLodge.proj1.Areas.Admin.Models.CSM;//dir for CSM models
using Microsoft.VisualStudio.Web.CodeGeneration.EntityFrameworkCore;
using System.Reflection;
using System.Linq;
using FastMember;

namespace MyLodge.proj1.Areas.Admin.Models.CSM //dir set
{
    public class AuraStats
    {
        [Newtonsoft.Json.JsonIgnore]
        private readonly RavenDBConnector ravendb = RavenDBConnector.Instance();
        //[Newtonsoft.Json.JsonIgnore]
        AuraLogs logs = new AuraLogs();
        [Newtonsoft.Json.JsonIgnore]
        SysDiagDebug d = new SysDiagDebug("AuraStats>>");
        [Newtonsoft.Json.JsonIgnore]
        private string className;
        public int Inquiries_incoming { get; set; }
        public int Inquiries_handled { get; set; }
        public int Inquiries_forwarded { get; set; }
        public int Total_received_today { get; set; }
        public int Total_handled { get; set; }
        public int Total_spam { get; set; }
        public bool AuraSystemOn { get; set; }
        public bool AutoUpdateFAQ { get; set; }
        public DateTime lastUpdated { get; set; }
        public AuraStats()
        {
            /*
            if (Read() is null) //causes stackoverflow when reading for 2nd time, instead check with instance
            {
                Inquiries_incoming = 0;
                Inquiries_handled = 0;
                Inquiries_forwarded = 0;
                Total_received_today = 0;
                Total_handled = 0;
                Total_spam = 0;
                AuraSystemOn = true;
                AutoUpdateFAQ = true;
                Update();
            }
            */
            className = this.GetType().Name;
        }
        public AuraStats(string className)
        {
            this.className = className;
        }
        public void Update()
        {
            //both create or overwrite
            AuraStats ar = new AuraStats();
            lastUpdated = DateTime.Today;
            ar = ar.Read();
            if(ar.lastUpdated < lastUpdated)
            {
                this.Initialize(); //resets to 0
            }
            var fieldNames = typeof(AuraStats).GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                            .Select(field => field.Name)
                            .ToArray<string>();//get all variable names
            string[] fields = { "Inquiries_incoming", "Inquiries_handled", "Inquiries_forwarded", "Total_received_today", "Total_handled", "Total_spam", "lastUpdated" };
            //ravendb.SaveDocument(className,fieldNames, this); //will overwrite
            ravendb.UpdateDocument<AuraStats>("AuraStats", fields, this); //will update
        }
        public void UpdateAuraSwitch()
        {
            string[] fields = { "AuraSystemOn"};
            AuraStats ar = new AuraStats();
            ar = ar.Read();
            if (ar.AuraSystemOn != this.AuraSystemOn)
            {
                ar.AuraSystemOn = this.AuraSystemOn;
                ravendb.UpdateDocument<AuraStats>(className, fields, ar);
            }
            
        }
        public void UpdateAuraFAQSwitch()
        {
            string[] fields = { "AutoUpdateFAQ" };
            AuraStats ar = new AuraStats();
            ar = ar.Read();
            if (ar.AutoUpdateFAQ != this.AutoUpdateFAQ)
            {
                ar.AutoUpdateFAQ = this.AutoUpdateFAQ;
                ravendb.UpdateDocument<AuraStats>(className, fields, ar);
            }
        }
        public AuraStats Read()
        {
            return ravendb.ReadDocument<AuraStats>(className);
        }
        public void Initialize()
        {
            Inquiries_incoming = 0;
            Inquiries_handled = 0;
            Inquiries_forwarded = 0;
            Total_received_today = 0;
            Total_handled = 0;
            Total_spam = 0;
            AuraSystemOn = true;
            AutoUpdateFAQ = true;
        }
        public List<string> GetChatDates()
        {
            //d.Out("GetchatDates!");
            
            List<string> dates = new List<string>();
            try
            {
                int i = 0;
                bool infinite = true;
                while (infinite)
                {
                    if (!(logs.ReadChatLogs(i) is null))
                    {
                        logs = logs.ReadChatLogs(i);
                        dates.Add(logs.Created.ToString("MM/dd/yyyy HH:mm:ss"));
                    }
                    else
                    {
                        return dates;
                    }
                    i++;
                }
            }
            catch (NullReferenceException)
            {

            }
            
            return null;
        }
        public List<int> GetChatIds()
        {
            //d.Out("GetChatIds!");
            
            List<int> chat_id = new List<int>();
            try
            {
                int i = 0;
                bool infinite = true;
                while (infinite)
                {
                    if (!(logs.ReadChatLogs(i) is null))
                    {
                        logs = logs.ReadChatLogs(i);
                        chat_id.Add(logs.Chat_id);
                    }
                    else
                    {
                        return chat_id;
                    }
                    i++;
                }
            }
            catch (NullReferenceException)
            {

            }
            
            return null;
        }
        public List<AuraLogs> getForwardedChats() {
            List<AuraLogs> ar = new List<AuraLogs>();
            foreach (AuraLogs l in logs.ReadAllLogsWithCondition(x => x.Forwarded == true))
            {
                if (l.Forwarded)
                {
                    ar.Add(l);
                }
            }
            return ar;
        }

        public T SetAlternateCast<T>(T obj)
        {
            var fieldNames = typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                            .Select(field => field.Name)
                            .ToList();
            dynamic thisClass = new AuraStats();
            AuraStats ar = new AuraStats();
            ar = ar.Read();
            thisClass = ObjectAccessor.Create(ar);
            //thisClass = thisClass.Read();

            for (int i = 0; i < fieldNames.Count; i++)
            {
                PropertyInfo Castproperty = obj.GetType().GetProperty(fieldNames[i]);
                //PropertyInfo Fromproperty = ar.GetType().GetProperty(fieldNames[i]);
                //d.Out("property->" + Castproperty.Name + " thisClass[" + i + "] -> " + thisClass[i]);
                Castproperty.SetValue(obj, thisClass[fieldNames[i]], null); 
            }
            return obj;
        }
        public override string ToString()
        {
            return "Incoming: " + Inquiries_incoming + " Handled: " + Inquiries_handled + " forwarded: " + Inquiries_forwarded + " Total_in: " + Total_received_today
                + " Total_processed:" + Total_handled + " Total_spam:" + Total_spam;
        }
    }
}
