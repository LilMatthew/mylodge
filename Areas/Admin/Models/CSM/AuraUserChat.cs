﻿using MyLodge.proj1.Areas.Admin.Models.CSM;
using MyLodge.proj1.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace MyLodge.proj1.Areas.Admin.Controllers
{
    public class AuraUserChat
    {
        [Newtonsoft.Json.JsonIgnore]
        private static readonly string reply_symbol = "r:";
        [Newtonsoft.Json.JsonIgnore]
        private static readonly string user_symbol = "p:";
        //[Newtonsoft.Json.JsonIgnore]
        //readonly AuraLogs log = new AuraLogs();
        [Newtonsoft.Json.JsonIgnore]
        private RavenDBConnector ravendb = RavenDBConnector.Instance();
        [Newtonsoft.Json.JsonIgnore]
        private static string id_pattern = "AuraUserChat/";

        public List<string> chatHistory;

        public AuraUserChat()
        {
            //user = new List<string>(); reply = new List<string>(); 
            chatHistory = new List<string>();
        }
        public void AddChatText(string user, string reply)
        {
            //this.user.Add(user);
            //this.reply.Add(reply);
            chatHistory.Add(reply_symbol + reply);
            chatHistory.Add(user_symbol + user);
        }
        public void AddChatText(string txt)
        {
            chatHistory.Add(txt); //need to provide 'r' or 'p'
        }
        public void SaveChat(int id)
        {
            //log.CreateChatLog(hasSpam, forwarded, regCustomer, username);//assigns chat_id
            ravendb.SaveDocument(id_pattern + id,this);
        }
        public void UpdateChat(int id)
        {
            string[] field = {"chatHistory"};
            ravendb.UpdateDocument<AuraUserChat>(id_pattern + id, field, this);
        }
        public void DeleteChat(int Chat_id)
        {
            //log.DeleteChatLogs(Chat_id);
            ravendb.DeleteDocument(id_pattern + Chat_id);
        }
        public AuraUserChat GetUserChat(int chat_id)
        {
            //log.ReadChatLogs(chat_id);
            AuraUserChat ac = new AuraUserChat();
            ac = ravendb.ReadDocument<AuraUserChat>(id_pattern + chat_id);
            return ac;
        }
        public AuraUserChat GetChat(int chat_id)
        {
            //log.ReadChatLogs(Chat_id);
            return ravendb.ReadDocument<AuraUserChat>(id_pattern + chat_id);
        }
        public override string ToString()
        {
            return chatHistory.ToString();
        }
    }
}
