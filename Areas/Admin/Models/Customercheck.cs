﻿using FastMember;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace MyLodge.proj1.Areas.Admin.Models
{
    public class Customercheck
    {
        [Newtonsoft.Json.JsonIgnore]
        Utils.RavenDBConnector raven = Utils.RavenDBConnector.Instance();
        [Newtonsoft.Json.JsonIgnore]
        private string className;
        public int custId { get; set; }
        public string custName { get; set; }
        public string phoneNo { get; set; }
        public string RoomNo { get; set; }
        public DateTime checkdate { get; set; }
        public DateTime checkOdate{ get; set; }

        public Customercheck()
        {
            this.className = this.GetType().Name;
        }
        public Customercheck(string className)
        {
            this.className = className;
        }

        public Customercheck(int custId, string custName, string phoneNo, string RoomNo, DateTime checkdate, DateTime checkOdate) : this(RoomNo)
        {
            this.custId = custId;
            this.custName = custName;
            this.phoneNo = phoneNo;
            this.RoomNo = RoomNo;
            this.checkdate = checkdate;
            this.checkOdate = checkOdate;
        }

        //for csv        
        public override string ToString()
        {
            String newLine = "";
            if (custId > 0)
            {
                newLine = "\n";
            }
            return newLine + custId + "," + custName + "," + phoneNo + "," + RoomNo + "," + checkdate + "," + checkOdate;
        }
        public List<int> GetCustomerID()
        {
            List<Customercheck> hr = new List<Customercheck>();
            hr = this.GetMint();
            hr.OrderByDescending(i => i.custId);

            return hr.Select(Customercheck => Customercheck.custId).ToList();
        }

        public List<Customercheck> GetMint()
        {
            return raven.QueryAll<Customercheck>();
        }


        public void Save()
        {
            this.custId = GetNewPK();
            raven.SaveDocument(this.GetType().Name + "/" + custId, this);
        }
        public Customercheck Read(int i)
        {
            return raven.ReadDocument<Customercheck>(className + "/" + i);
        }
        public int GetNewPK()
        {
            Customercheck obj = new Customercheck();
            int i = 0;
            try
            {
                bool infinite = true;
                while (infinite)
                {

                    if (!(Read(i) is null))
                    {
                        obj = raven.ReadDocument<Customercheck>(this.GetType().Name + "/" + i);
                        i++;
                    }
                    else
                    {
                        return i;
                        //break;
                    }
                }
            }
            catch (NullReferenceException)
            {

            }
            return i;
        }


        public List<Customercheck> GetAllCustomerdetails()
        {

            List<Customercheck>listCustomercheck = new List<Customercheck>();
            Customercheck obj = new Customercheck();
            bool infinite = true;
            int i = 0;
            while (infinite)
            {

                if (!(Read(i) is null))
                {
                    obj = raven.ReadDocument<Customercheck>(className + "/" + i);

                    listCustomercheck.Add(obj);
                    i++;
                }
                else
                {
                    return listCustomercheck;
                }
            }
            return listCustomercheck;
        }


        //GetAllCustomernames
        public List<string> GetCustomerNames()
        {
            List<Customercheck> hr = new List<Customercheck>();
            hr = this.GetAllCustomerdetails();

            return hr.Select(Customercheck => Customercheck.custName).ToList();
        }

        public List<string> Getphonenumber()
        {
            List<Customercheck> hr = new List<Customercheck>();
            hr = this.GetAllCustomerdetails();

            return hr.Select(Customercheck => Customercheck.phoneNo).ToList();
        }
  
        public List<string> getRoomNumber()
        {
            List<Customercheck> hr = new List<Customercheck>();
            hr = this.GetAllCustomerdetails();

            return hr.Select(Customercheck => Customercheck.RoomNo).ToList();
        }


        public List<DateTime> GetCheckinData()
        {
            List<Customercheck> hr = new List<Customercheck>();
            hr = this.GetAllCustomerdetails();

            return hr.Select(Customercheck => Customercheck.checkdate).ToList();
        }
        //GetAllMemberUserID
        public List<DateTime> Getcheckoutdata()
        {
            List<Customercheck> hr = new List<Customercheck>();
            hr = this.GetAllCustomerdetails();

            return hr.Select(Customercheck => Customercheck.checkOdate).ToList();
        }


        //update
        public void UpdateCus() { 
            String[] variables = { "custName", "phoneNo", "RoomNo", "checkdate", "checkOdate" };
            raven.UpdateDocument<Customercheck>(className + "/" + this.custId, variables, this);
        }
        public void Delete(int i)
        {
            raven.DeleteDocument(className + "/" + i);
        }


        public Customercheck GetMintById(int cusId)
        {
            List<Customercheck> c = new List<Customercheck>();
            c = raven.QueryWhere<Customercheck>(x => x.custId == custId).ToList();
            try
            {
                if (c[0] is null)
                {
                    return null;
                }
            }
            catch (IndexOutOfRangeException)
            {
                return null;
            }
            catch (ArgumentOutOfRangeException)
            {
                return null;
            }
            return c[0];
        }

        public T SetAlternateCast<T>(T obj, int id)
        {
            var fieldNames = typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                            .Select(field => field.Name)
                            .ToList();
            dynamic thisClass = new Customercheck();
            Customercheck ar = new Customercheck();
            ar = ar.Read(id);
            thisClass = ObjectAccessor.Create(ar);
            //thisClass = thisClass.Read();

            for (int i = 0; i < fieldNames.Count; i++)
            {
                PropertyInfo Castproperty = obj.GetType().GetProperty(fieldNames[i]);
                //PropertyInfo Fromproperty = ar.GetType().GetProperty(fieldNames[i]);
                //d.Out("property->" + Castproperty.Name + " thisClass[" + i + "] -> " + thisClass[i]);
                Castproperty.SetValue(obj, thisClass[fieldNames[i]], null);
            }
            return obj;
        }
    }
}

