﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyLodge.proj1.Utils;
using System.Web;

namespace MyLodge.proj1.Areas.Admin.Models
{
    public class EmployeeLeaveForm
    {
        [Newtonsoft.Json.JsonIgnore]
        RavenDBConnector raven = RavenDBConnector.Instance();
        [Newtonsoft.Json.JsonIgnore]
        private string className;
        public string empID { get; set; }
       // public string nic { get; set; }
        //public int Reservation_id{;
        public DateTime apply_date { get; set; }
        public DateTime start_date { get; set; }

        public DateTime end_date { get; set; }
        // public DateTime end_date { get; set; }

        // public DateTime Required_date { get; set; }
        //public string pay_type { get; set; }
        //public Boolean Online_pay { get; set; }
        //public Boolean Arrival_pay { get; set; }

        public EmployeeLeaveForm()
        {

        }
        public EmployeeLeaveForm(string className)
        {
            this.className = className;
        }

        /*public Hallreservation(int reservation_id, string name, DateTime required_date, Boolean Online_pay, Boolean Arrival_pay)
        {
            //this.Reservation_id = reservation_id;
            this.Name = name;
            this.Required_date = required_date;
            this.Online_pay = Online_pay;
            this.Arrival_pay = Arrival_pay;
        }*/
        public void Save()
        {
            this.empID = "" + GetNewPK();
            raven.SaveDocument(this.GetType().Name + "/" + empID, this);
        }
        public EmployeeLeaveForm Read(int i)
        {
            return raven.ReadDocument<EmployeeLeaveForm>(this.GetType().Name + "/" + i);
        }
        public int GetNewPK()
        {
            EmployeeLeaveForm obj = new EmployeeLeaveForm();
            int i = 0;
            try
            {
                bool infinite = true;
                while (infinite)
                {

                    if (!(Read(i) is null))
                    {
                        obj = raven.ReadDocument<EmployeeLeaveForm>(this.GetType().Name + "/" + i);
                        i++;
                    }
                    else
                    {
                        return i;
                        //break;
                    }
                }
            }
            catch (NullReferenceException)
            {

            }
            return i;
        }
    }
}
