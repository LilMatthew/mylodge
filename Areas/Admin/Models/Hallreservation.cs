﻿using MyLodge.proj1.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyLodge.proj1.Models
{
    public class Hallreservation

    {
        [Newtonsoft.Json.JsonIgnore]
        RavenDBConnector raven = RavenDBConnector.Instance();
        [Newtonsoft.Json.JsonIgnore]
        private string className;
        SysDiagDebug d = new SysDiagDebug();
        public int Reservation_id { get; set; }
        //public int Reservation_id{;
        public string Name { get; set; }
        public DateTime Required_date { get; set; }
        public string email { get; set; }
        public string pay_type { get; set; }
        public string type { get; set; }



        public Hallreservation()
        {
            this.className = this.GetType().Name;
        }
        public Hallreservation(string className)
        {
            this.className = className;
        }

        public Hallreservation(int reservation_id, string name, DateTime Required_date,string email,string Pay_type)
        {
            //this.Reservation_id = reservation_id;
            this.Name = name;
            this.Required_date = Required_date;
            this.email = email;
            this.pay_type = pay_type;
        }
        public void Save()
        {
            this.Reservation_id = GetNewPK();
            raven.SaveDocument(this.GetType().Name+"/" + Reservation_id, this);
        }
        public Hallreservation Read(int i)
        {
            return raven.ReadDocument<Hallreservation>(className + "/" + i);
        }
        public int GetNewPK()
        {
            Hallreservation obj = new Hallreservation();
            int i = 0;
            try
            {
                bool infinite = true;
                while (infinite)
                {

                    if (!(Read(i) is null))
                    {
                        obj = raven.ReadDocument<Hallreservation>(this.GetType().Name + "/" + i);
                        i++;
                    }
                    else
                    {
                        return i;
                        //break;
                    }
                }
            }
            catch (NullReferenceException)
            {

            }
            return i;
        }
        public List<Hallreservation> GetAllHallReservations()
        {
          
            List<Hallreservation> listHalls = new List<Hallreservation>();
            Hallreservation obj = new Hallreservation();
            bool infinite = true;
            int i = 0;
            while (infinite)
            {

                if (!(Read(i) is null))
                {
                    obj = raven.ReadDocument<Hallreservation>(className + "/" + i);
                 
                    listHalls.Add(obj);
                    i++;
                }
                else
                {
                    return listHalls;
                }
            }
            return listHalls;
        }
        //GetAllHallReservationNames
        public List<string> GetHallReNames()
        {
            List<Hallreservation> hr = new List<Hallreservation>();           
            hr = this.GetAllHallReservations();
           
            return hr.Select(Hallreservation => Hallreservation.Name).ToList();
        }

        public List<DateTime> GetHallReDate()
        {
            List<Hallreservation> hr = new List<Hallreservation>();
            hr = this.GetAllHallReservations();

            return hr.Select(Hallreservation => Hallreservation.Required_date).ToList();
        }

        public void Delete(int i)
        {
            raven.DeleteDocument(className + "/" +i);
        }
    }
}

    