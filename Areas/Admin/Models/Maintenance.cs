﻿using FastMember;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace MyLodge.proj1.Areas.Admin.Models
{
    public class Maintenance
    {
        [Newtonsoft.Json.JsonIgnore]
        Utils.RavenDBConnector raven = Utils.RavenDBConnector.Instance();
        [Newtonsoft.Json.JsonIgnore]
        private string className;
        public int Maintenanceid { get; set; }
        public string RoomNo { get; set; }
        public string Items { get; set; }
        public string Toiletries { get; set; }
        public DateTime LcleanDate { get; set; }
        public DateTime NcleanDate { get; set; }

        public Maintenance()
        {
            this.className = this.GetType().Name;
        }
        public Maintenance(string className)
        {
            this.className = className;
        }

        public Maintenance(int Maintenanceid, string RoomNo, DateTime NcleanDate, DateTime LcleanDate, string Items, string Toiletrie) : this(RoomNo)
        {
            this.Maintenanceid = Maintenanceid;
            this.RoomNo = RoomNo;
            this.Items = Items;
            this.Toiletries = Toiletries;
            this.NcleanDate = NcleanDate;
            this.LcleanDate = LcleanDate;
        }
        //for csv        
        public override string ToString()
        {
            String newLine = "";
            if (Maintenanceid > 0)
            {
                newLine = "\n";
            }
            return newLine + Maintenanceid + "," + RoomNo + "," + Items + "," + Toiletries + "," + LcleanDate + "," + NcleanDate;
        }
        public List<int> GetMaintID()
        {
            List<Maintenance> hr = new List<Maintenance>();
            //hr = this.GetMint();
            hr = this.GetAllMaintenancedetails();
            hr.OrderByDescending(i => i.Maintenanceid);
            return hr.Select(Maintenance => Maintenance.Maintenanceid).ToList();
        }

        public List<Maintenance> GetMint()
        {
            return raven.QueryAll<Maintenance>();
        }

        public void Save()
        {
            this.Maintenanceid = GetNewPK();
            raven.SaveDocument(this.GetType().Name + "/" + Maintenanceid, this);
        }
        public Maintenance Read(int i)
        {
            return raven.ReadDocument<Maintenance>(className + "/" + i);
        }
        public int GetNewPK()
        {
            Maintenance obj = new Maintenance();
            int i = 0;
            try
            {
                bool infinite = true;
                while (infinite)
                {

                    if (!(Read(i) is null))
                    {
                        obj = raven.ReadDocument<Maintenance>(this.GetType().Name + "/" + i);
                        i++;
                    }
                    else
                    {
                        return i;
                        //break;
                    }
                }
            }
            catch (NullReferenceException)
            {

            }
            return i;
        }


        public List<Maintenance> GetAllMaintenancedetails()
        {

            return raven.QueryAll<Maintenance>();
        }


        //GetAllRoomNo
        public List<string> GetmemberReRoomNo()
        {
            List<Maintenance> hr = new List<Maintenance>();
            hr = this.GetAllMaintenancedetails();

            return hr.Select(Maintenance => Maintenance.RoomNo).ToList();
        }
        //GetAllMemberDOB
        public List<DateTime> GetmemberReLcleanDate()
        {
            List<Maintenance> hr = new List<Maintenance>();
            hr = this.GetAllMaintenancedetails();

            return hr.Select(Maintenance => Maintenance.LcleanDate).ToList();
        }
        //GetAllMemberDOB
        public List<DateTime> GetmemberReNcleanDate()
        {
            List<Maintenance> hr = new List<Maintenance>();
            hr = this.GetAllMaintenancedetails();

            return hr.Select(Maintenance => Maintenance.NcleanDate).ToList();
        }

        //GetAllMemberUserID
        public List<string> GetmemberReItems()
        {
            List<Maintenance> hr = new List<Maintenance>();
            hr = this.GetAllMaintenancedetails();

            return hr.Select(Maintenance => Maintenance.Items).ToList();
        }
        //GetAllMemberUserID
        public List<string> GetmemberReToiletries()
        {
            List<Maintenance> hr = new List<Maintenance>();
            hr = this.GetAllMaintenancedetails();

            return hr.Select(Maintenance => Maintenance.Toiletries).ToList();
        }



        //update
        public void UpdateMaintenance()
        {
            String[] variables = { "RoomNo", "Items", "Toiletries", "LcleanDate", "NcleanDate" };
            raven.UpdateDocument<Maintenance>(className + "/" + this.Maintenanceid, variables, this);
        }
        public void Delete(int i)
        {
            raven.DeleteDocument(className + "/" + i);
        }


        public Maintenance GetMintById(int maintenanceID)
        {
            List<Maintenance> c = new List<Maintenance>();
            c = raven.QueryWhere<Maintenance>(x => x.Maintenanceid == maintenanceID).ToList();
            try
            {
                if (c[0] is null)
                {
                    return null;
                }
            }
            catch (IndexOutOfRangeException)
            {
                return null;
            }
            catch (ArgumentOutOfRangeException)
            {
                return null;
            }
            return c[0];
        }

        public T SetAlternateCast<T>(T obj, int id)
        {
            var fieldNames = typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                            .Select(field => field.Name)
                            .ToList();
            dynamic thisClass = new Maintenance();
            Maintenance ar = new Maintenance();
            ar = ar.Read(id);
            thisClass = ObjectAccessor.Create(ar);
            //thisClass = thisClass.Read();

            for (int i = 0; i < fieldNames.Count; i++)
            {
                PropertyInfo Castproperty = obj.GetType().GetProperty(fieldNames[i]);
                //PropertyInfo Fromproperty = ar.GetType().GetProperty(fieldNames[i]);
                //d.Out("property->" + Castproperty.Name + " thisClass[" + i + "] -> " + thisClass[i]);
                Castproperty.SetValue(obj, thisClass[fieldNames[i]], null);
            }
            return obj;
        }
    }
}

