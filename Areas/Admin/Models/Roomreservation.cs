﻿using FastMember;
using MyLodge.proj1.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace MyLodge.proj1.Models
{
    public class Roomreservation
    {
        [Newtonsoft.Json.JsonIgnore]
        RavenDBConnector raven = RavenDBConnector.Instance();
        [Newtonsoft.Json.JsonIgnore]
        private string className;

        [Newtonsoft.Json.JsonIgnore]
        private SysDiagDebug d = new SysDiagDebug();
        public int reservation_id { get; set; }

        public string name { get; set; }
        public string email { get; set; }
        public DateTime check_in { get; set; }
        public DateTime check_out { get; set; }
        public string pay_type { get; set; }
        public string days { get; set; }
        public string  rooms{ get; set; }
        public string type { get; set; }

        public Roomreservation()
        {
            this.className = this.GetType().Name;
        }
        public Roomreservation(string className)
        {
            this.className = className;
        }

        public Roomreservation(int reservation_id, string name,string email, DateTime check_in, DateTime check_out , string pay_type,string days,string rooms)
        {
            this.reservation_id = reservation_id;
            this.name = name;
            this.email = email;
            this.check_in = check_in;
            this.check_out = check_out;
            this.pay_type = pay_type;
            this.days = days;
            this.rooms = rooms;
        }
        public void Save()
        {
            this.reservation_id = GetNewPK();
            raven.SaveDocument(this.GetType().Name + "/" + reservation_id, this);
        }

        //update
        public void UpdateRoom()
        {
            String[] variables = { "check_in", "check_out", "days", "rooms" };
            raven.UpdateDocument<Roomreservation>(className + "/" + this.reservation_id, variables, this);
        }


        public Roomreservation GetRoomReservationByreservation_id(int roomid)
        {
            List<Roomreservation> c = new List<Roomreservation>();
            c = raven.QueryWhere<Roomreservation>(x => x.reservation_id == roomid).ToList();
            try
            {
                if (c[0] is null)
                {
                    return null;
                }
            }
            catch (IndexOutOfRangeException)
            {
                return null;
            }
            catch (ArgumentOutOfRangeException)
            {
                return null;
            }
            return c[0];
        }


        public Roomreservation Read(int i)
        {
            return raven.ReadDocument<Roomreservation>(className + "/" + i);
        }
        public int GetNewPK()
        {
            Roomreservation obj = new Roomreservation();
            int i = 0;
            try
            {
                bool infinite = true;
                while (infinite)
                {

                    if (!(Read(i) is null))
                    {
                        obj = raven.ReadDocument<Roomreservation>(this.GetType().Name + "/" + i);
                        i++;
                    }
                    else
                    {
                        return i;
                        //break;
                    }
                }
            }
            catch (NullReferenceException)
            {

            }
            return i;
        }
        public List<Roomreservation> GetAllRoomReservations()
        {
            return raven.QueryAll<Roomreservation>();
        }

        public List<Roomreservation> GetAllRoomReservationsByEmail(String Email)
        {
            return raven.QueryWhere<Roomreservation>(x => x.email == Email);
        }

        public List<string> GetRoomRePay()
        {
            List<Roomreservation> hr = new List<Roomreservation>();
            hr = this.GetAllRoomReservations();

            return hr.Select(Roomreservation => Roomreservation.pay_type).ToList();
        }

        


        //GetAllHallReservationNames
        public List<string> GetRoomReNames()
        {
            List<Roomreservation> hr = new List<Roomreservation>();
            hr = this.GetAllRoomReservations();
            hr = hr.OrderByDescending(i => i.reservation_id).ToList();
            return hr.Select(Roomreservation => Roomreservation.name).ToList();
        }

        public List<string> GetRoomReEmail()
        {
            List<Roomreservation> hr = new List<Roomreservation>();
            hr = this.GetAllRoomReservations();
            hr = hr.OrderByDescending(i => i.reservation_id).ToList();
            return hr.Select(Roomreservation => Roomreservation.email).ToList();
        }

        public List<DateTime> GetRoomReCheckin()
        {
            List<Roomreservation> hr = new List<Roomreservation>();
            hr = this.GetAllRoomReservations();
            hr = hr.OrderByDescending(i => i.reservation_id).ToList();
            return hr.Select(Roomreservation => Roomreservation.check_in).ToList();
        }

        public List<DateTime> GetRoomReCheckout()
        {
            List<Roomreservation> hr = new List<Roomreservation>();
            hr = this.GetAllRoomReservations();
            hr = hr.OrderByDescending(i => i.reservation_id).ToList();
            return hr.Select(Roomreservation => Roomreservation.check_out).ToList();
        }
        public List<String> GetRoomReQuantity()
        {
            List<Roomreservation> hr = new List<Roomreservation>();
            hr = this.GetAllRoomReservations();
            hr = hr.OrderByDescending(i => i.reservation_id).ToList();
            return hr.Select(Roomreservation => Roomreservation.rooms).ToList();
        }

        public List<String> GetRoomReDays()
        {
            List<Roomreservation> hr = new List<Roomreservation>();
            hr = this.GetAllRoomReservations();
            hr = hr.OrderByDescending(i => i.reservation_id).ToList();
            return hr.Select(Roomreservation => Roomreservation.days).ToList();
        }

        public List<String> GetRoomRePayment()
        {
            List<Roomreservation> hr = new List<Roomreservation>();
            hr = this.GetAllRoomReservations();
            hr = hr.OrderByDescending(i => i.reservation_id).ToList();
            return hr.Select(Roomreservation => Roomreservation.pay_type).ToList();
        }

        public List<String> GetRoomReType()
        {
            List<Roomreservation> hr = new List<Roomreservation>();
            hr = this.GetAllRoomReservations();
            hr = hr.OrderByDescending(i => i.reservation_id).ToList();
            return hr.Select(Roomreservation => Roomreservation.type).ToList();
        }

        //for csvl — reference', 
        public override string ToString()
        {
            String newLine = "";
            if (reservation_id > 0)
            {
                newLine = "\n";
            }
            return newLine + reservation_id + "," + name + "," + email + "," + check_in + "," + check_out + "," + pay_type + "," + days + "," + rooms;
        }
         public void Delete(int i)
        {
            raven.DeleteDocument(className + "/" + i);
        }

        public T SetAlternateCast<T>(T obj, int j)
        {
            var fieldNames = typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                            .Select(field => field.Name)
                            .ToList();
            dynamic thisClass = new Roomreservation();
            Roomreservation ar = new Roomreservation();
            ar = ar.Read(j);
            thisClass = ObjectAccessor.Create(ar);
            //thisClass = thisClass.Read();

            for (int i = 0; i < fieldNames.Count; i++)
            {
                PropertyInfo Castproperty = obj.GetType().GetProperty(fieldNames[i]);
                //PropertyInfo Fromproperty = ar.GetType().GetProperty(fieldNames[i]);
                //d.Out("property->" + Castproperty.Name + " thisClass[" + i + "] -> " + thisClass[i]);
                Castproperty.SetValue(obj, thisClass[fieldNames[i]], null);
            }
            return obj;
        }
    }
}

    


