﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyLodge.proj1.Utils;
using System.Web;
using System.Reflection;
using FastMember;
using SodiumSecurity;

namespace MyLodge.proj1.Areas.Admin.Models
{
    public class employeeRegistration
    {

        //new code
        [Newtonsoft.Json.JsonIgnore]
        RavenDBConnector raven = RavenDBConnector.Instance();
        [Newtonsoft.Json.JsonIgnore]
        private string className = "employeeRegistration";
        [Newtonsoft.Json.JsonIgnore]
        Argon2IDUsingLibsodium security = new Argon2IDUsingLibsodium();
        public int eid { get; set; }
        public string nic { get; set; }
        //public int Reservation_id{;
        public string fname { get; set; }
        public string address { get; set; }
        public string email { get; set; }
        public int phone { get; set; }
        public string feild_of_work { get; set; }
        //ignore password field
        [Newtonsoft.Json.JsonIgnore]
        public string password { get; set; }

        public byte[] passwordHash;
        public byte[] passwordSalt;

        public employeeRegistration()
        {
            this.className = this.GetType().Name;
        }
        public employeeRegistration(string className)
        {
            this.className = className;
        }
        //Argon2ID
        public void SetPassword(string password)
        {
            this.passwordSalt = security.GetGeneratedSalt(password);
            SetHash();
        }
        public void SetHash()
        {
            this.passwordHash = security.GetGeneratedHashBytes(password, passwordSalt);
        }
        public Boolean PasswordIsMatching(string password)
        {
            byte[] newH = security.GetGeneratedHashBytes(password, this.passwordSalt);
            if(newH.SequenceEqual(passwordHash))
            {
                return true;
            }
            return false;
        }
        //End of Argon2ID
        public void Save()
        {
            //assign pk
            this.eid = GetNewPK();
            SetPassword(password);//for libsodium
            CheckFieldOfWork();//prevents crashing into non-implemented fields
            raven.SaveDocument(this.GetType().Name + "/E" + eid, this);
        }
        public void CheckFieldOfWork()
        {
            Boolean setTypeToEmu = true;
            string[] available = { "CSM","Stock","GM","Mint","EIS_Emu"};

            foreach(string type_of_field in available)
            {
                if(this.feild_of_work == type_of_field)
                {
                    setTypeToEmu = false;
                }
            }
            if (setTypeToEmu)
            {
                this.feild_of_work = available[4];
            }
        }
        public string GetMeaningfulFieldOfWork()
        {
            string[] available = { "CSM", "Stock", "GM", "Mint", "EIS_Emu" };
            string[] set = { "Customer Support Manager", "Stock Manager", "General Manager","Maintenance Manager", "Employee" };
            for(int i = 0; i < available.Length; i++)
            {
                if(available[i] == this.feild_of_work)
                {
                    return set[i];
                }
            }
            return "Employee";
        }

        public employeeRegistration GetEmployeeByNIC(String nic)
        {
            List<employeeRegistration> empl = new List<employeeRegistration>();
            empl = raven.QueryWhere<employeeRegistration>(x => x.nic == nic);
            try
            {
                return empl[0];
            }
            catch (ArgumentNullException)
            {
                return null;
            }
            catch (ArgumentOutOfRangeException)
            {
                return null;
            }
        }
        public employeeRegistration Read(int i)
        {
            //return raven.ReadDocument<employeeRegistration>(this.GetType().Name + "/" + i);
            return raven.ReadDocument<employeeRegistration>(className + "/E" + i);
        }

        public employeeRegistration Read()
        {
            //return raven.ReadDocument<employeeRegistration>(this.GetType().Name + "/" + i);
            return raven.ReadDocument<employeeRegistration>(className + "/E" + this.eid);
        }
        public int GetNewPK()
        {
            employeeRegistration obj = new employeeRegistration();
            int i = 0;
            try
            {
                bool infinite = true;
                while (infinite)
                {

                    if (!(Read(i) is null))
                    {
                        obj = raven.ReadDocument<employeeRegistration>(this.GetType().Name + "/E100" + i);
                        i++;
                    }
                    else
                    {
                        return i;
                        //break;
                    }
                }
            }
            catch (NullReferenceException)
            {

            }
            return i;
        }
        //Requires update v1
        public List<employeeRegistration> GetEmployeeDetails()
        {
            return raven.QueryAll<employeeRegistration>();
        }
        //GetAllHallReservationNames
        /* public List<string> GetEmployeeNames()
         {
             List<employeeRegistration> hr = new List<employeeRegistration>();
             hr = this.GetEmployeeDetails();
             return hr.Select(employeeRegistration => employeeRegistration.fname).ToList();
         }*/
        public List<string> GetEmployeeNames()
        {
            List<employeeRegistration> hr = new List<employeeRegistration>();
            hr = this.GetEmployeeDetails();
            hr = hr.OrderBy(i => i.eid).ToList();
            return hr.Select(employeeRegistration => employeeRegistration.fname).ToList();
        }

        public List<string> GetEmployeeFeildofWorking()
        {
            List<employeeRegistration> hr = new List<employeeRegistration>();
            hr = this.GetEmployeeDetails();
            hr = hr.OrderBy(i => i.eid).ToList();
            return hr.Select(employeeRegistration => employeeRegistration.feild_of_work).ToList();
        }

        public List<int> GetEmployeeID()
        {
            List<employeeRegistration> hr = new List<employeeRegistration>();
            hr = this.GetEmployeeDetails();
            hr = hr.OrderBy(i => i.eid).ToList();
            return hr.Select(employeeRegistration => employeeRegistration.eid).ToList();
        }

        public List<string> GetEmployeeNIC()
        {
            List<employeeRegistration> hr = new List<employeeRegistration>();
            hr = this.GetEmployeeDetails();
            hr = hr.OrderBy(i => i.eid).ToList();
            return hr.Select(employeeRegistration => employeeRegistration.nic).ToList();
        }

        public List<string> GetEmployeeAddress()
        {
            List<employeeRegistration> hr = new List<employeeRegistration>();
            hr = this.GetEmployeeDetails();
            hr = hr.OrderBy(i => i.eid).ToList();
            return hr.Select(employeeRegistration => employeeRegistration.address).ToList();
        }

        public List<string> GetEmployeeEmail()
        {
            List<employeeRegistration> hr = new List<employeeRegistration>();
            hr = this.GetEmployeeDetails();
            hr = hr.OrderBy(i => i.eid).ToList();
            return hr.Select(employeeRegistration => employeeRegistration.email).ToList();
        }

        public List<int> GetEmployeePhone()
        {
            List<employeeRegistration> hr = new List<employeeRegistration>();
            hr = this.GetEmployeeDetails();
            hr = hr.OrderBy(i => i.eid).ToList();
            return hr.Select(employeeRegistration => employeeRegistration.phone).ToList();
        }

        public T SetAlternateCast<T>(T obj, int id)
        {
            var fieldNames = typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                            .Select(field => field.Name)
                            .ToList();
            dynamic thisClass = new employeeRegistration();
            employeeRegistration ar = new employeeRegistration();
            ar = ar.Read(id);
            // ar = ar.Read(nic);
            thisClass = ObjectAccessor.Create(ar);
            //thisClass = thisClass.Read();

            for (int i = 0; i < fieldNames.Count; i++)
            {
                PropertyInfo Castproperty = obj.GetType().GetProperty(fieldNames[i]);
                //PropertyInfo Fromproperty = ar.GetType().GetProperty(fieldNames[i]);
                //d.Out("property->" + Castproperty.Name + " thisClass[" + i + "] -> " + thisClass[i]);
                Castproperty.SetValue(obj, thisClass[fieldNames[i]], null);
            }
            return obj;
        }

        //code added delete
        public void Delete(int i)
        {
            raven.DeleteDocument(className + "/E" + i);
        }
        public T SetAlternateCast<T>(T obj)
        {
            var fieldNames = typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                            .Select(field => field.Name)
                            .ToList();
            dynamic thisClass = new employeeRegistration();
            employeeRegistration ar = new employeeRegistration();
            ar = ar.Read();
            thisClass = ObjectAccessor.Create(ar);
            //thisClass = thisClass.Read();

            for (int i = 0; i < fieldNames.Count; i++)
            {
                PropertyInfo Castproperty = obj.GetType().GetProperty(fieldNames[i]);
                //PropertyInfo Fromproperty = ar.GetType().GetProperty(fieldNames[i]);
                //d.Out("property->" + Castproperty.Name + " thisClass[" + i + "] -> " + thisClass[i]);
                Castproperty.SetValue(obj, thisClass[fieldNames[i]], null);
            }
            return obj;
        }

        //add

        //update employee

        public void UpdateEmp()
        {
            String[] variables = { "nic", "fname", "address", "email", "phone" };
            CheckFieldOfWork();//prevents crashing into non-implemented fields
            raven.UpdateDocument<employeeRegistration>(className + "/E" + this.eid, variables, this);
        }

        public void UpdateEmpS()
        {
            String[] variables = { "fname", "feild_of_work" };
            CheckFieldOfWork();//prevents crashing into non-implemented fields
            raven.UpdateDocument<employeeRegistration>(className + "/E" + this.eid, variables, this);
        }
        public void ResetPassword(string newPassword)
        {
            String[] variables = { "passwordHash", "passwordSalt" };
            SetPassword(newPassword);
            raven.UpdateDocument<employeeRegistration>(className + "/E" + this.eid, variables, this);
        }

        public override string ToString()
        {
            String newLine = "";
            if (eid > 0)
            {
                newLine = "\n";
            }
            return newLine + "EID: " + (eid + 1) + "   Name: " + fname + "   Feild of Work: " + feild_of_work; 
        }


    }
}
