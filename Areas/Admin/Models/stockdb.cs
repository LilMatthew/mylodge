﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyLodge.proj1.Utils;
using System.Web;

namespace MyLodge.proj1.Areas.Admin.Models
{
    public class stockdb
    {
        [Newtonsoft.Json.JsonIgnore]
        RavenDBConnector raven = RavenDBConnector.Instance();
        [Newtonsoft.Json.JsonIgnore]
        private string className = "stockdb";
        public int Item_Code { get; set; }
        public string Item_Name { get; set; }
        public string Brand_Name { get; set; }   
        public string Amount { get; set; }

        public int TotalQ = 0;

        public int Reorder_Level { get; set; }
       
        public stockdb()
        {

        }
        public stockdb(string className)
        {
            this.className = className;}

        public stockdb(int icode, string iname, string bname, string amount, int reorderLevel)
        {   this.Item_Code = icode;
            this.Item_Name = iname;
            this.Brand_Name = bname;
            this.Amount = amount;
            this.Reorder_Level = reorderLevel;}

        public void Save()
        {
            this.Item_Code = GetNewPK();
            raven.SaveDocument(this.GetType().Name + "/" + Item_Code, this);
        }

        public stockdb Read(int i)
        {
            return raven.ReadDocument<stockdb>(this.GetType().Name + "/" + i);}

       
        public int GetNewPK()
        {
            stockdb obj = new stockdb();
            int i = 0;
            try
            {
                bool infinite = true;
                while (infinite)
                {
                    if (!(Read(i) is null))    {
                       // obj = raven.ReadDocument<stockdb>(this.GetType().Name + "/" + i);
                        i++;
                    }
                    else{
                        return i;
                        //break;
                    }
                }
            }
            catch (NullReferenceException) {
                return i;
            }
            return 0;
        }


        
        public List<stockdb> GetAddItems()
        {
            return raven.QueryAll<stockdb>();
        }

        public List<int> GetAddItemCode()
        {
            List<stockdb> hr = new List<stockdb>();
            hr = this.GetAddItems();
            hr = hr.OrderBy(i=>i.Item_Code).ToList();
            return hr.Select(stockdb => stockdb.Item_Code).ToList();
        }

        public List<string>GetAddItemName()
        {
            List<stockdb> hr = new List<stockdb>();
            hr = this.GetAddItems();
            hr = hr.OrderBy(i => i.Item_Code).ToList();
            return hr.Select(stockdb => stockdb.Item_Name).ToList();
        }
        public List<string> GetAddItemBrand()
        {
            List<stockdb> hr = new List<stockdb>();
            hr = this.GetAddItems();
            hr = hr.OrderBy(i => i.Item_Code).ToList();
            return hr.Select(stockdb => stockdb.Brand_Name).ToList();
        }

        public List<string> GetAddItemAmount()
        {
            List<stockdb> hr = new List<stockdb>();
            hr = this.GetAddItems();
            hr = hr.OrderBy(i => i.Item_Code).ToList();
            return hr.Select(stockdb => stockdb.Amount).ToList();
        }

        public List<int> GetAddItemReorderlevel()
        {
            List<stockdb> hr = new List<stockdb>();
            hr = this.GetAddItems();
            hr = hr.OrderBy(i => i.Item_Code).ToList();
            return hr.Select(stockdb => stockdb.Reorder_Level).ToList();
        }

        public List<int> GetAddItemTotal()
        {
            List<stockdb> hr = new List<stockdb>();
            hr = this.GetAddItems();
            hr = hr.OrderBy(i => i.Item_Code).ToList();
            return hr.Select(stockdb => stockdb.TotalQ).ToList();
        }


        //update
        public void UpdateItem(int id)
         {
            SysDiagDebug d = new SysDiagDebug();
            d.Out("model.Update id" + this.Item_Code + " amount =" + Amount + " Item name ="+Item_Name);
            d.Out("doc= "+ className + "/" +id );
            String[] variables = { "Item_Name", "Brand_Name", "Reorder_Level","Amount" };
             raven.UpdateDocument<stockdb>(className + "/" + id, variables, this);
         }

        public void UpdateTotal()
        {
            SysDiagDebug d = new SysDiagDebug();
            d.Out("model.Update id" + this.Item_Code + " amount =" + TotalQ );

            String[] variables = { "TotalQ" };
            raven.UpdateDocument<stockdb>(className + "/" + this.Item_Code, variables, this);

        }

        

        public void Delete(int i)
        {
            raven.DeleteDocument(className + "/" + i);
        }


        public Boolean DoesItemExist(int itemc)
        {
            stockdb i = new stockdb();
            i = i.Read(itemc);
            if(i is null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /*public Boolean lowstock(int low)
        {
            stockdb l = new stockdb();
            l = l.Read(low);
            if(l <= Reorder_Level)
            {
                return false;
               
            }
        }*/








    }

   
}
