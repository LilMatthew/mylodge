﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Session;
using MyLodge.proj1.Areas.Admin.Controllers;
using MyLodge.proj1.Areas.Admin.Models.CSM;
using MyLodge.proj1.Models.Aura;
using MyLodge.proj1.Utils;
using Newtonsoft.Json;

namespace MyLodge.proj1.Controllers
{   
    public class AuraChatFaqController : Controller
    {
        SysDiagDebug d = new SysDiagDebug();
        [HttpGet]
        public IActionResult Chat()
        {
            AuraStats astats = new AuraStats();
            astats = astats.Read();//loads aura stats
            HttpContext.Session.SetObject("chatModel", null);
            if (!astats.AuraSystemOn)
            {
                //aura turned off
                return View("~/Views/Reservation/ContactUs.cshtml");
            }
            var chatModel = new AuraUserChat();
            //List<string> chatHistory = new List<string>();
            chatModel.chatHistory.Add("r:Hello! This is "+Aura.Name+", how can I help you?");
            d.Out(chatModel.ToString());
            HttpContext.Session.SetObject("chatModel", chatModel);
            HttpContext.Session.SetObject("auraModel", null);
            ViewBag.Collection = chatModel.chatHistory;
            return View("~/Views/Aura/Chat.cshtml");
        }
        [HttpPost]
        [HttpGet]
        public IActionResult GetResult(string msg)
        {
            SysDiagDebug d = new SysDiagDebug();
            var chatModel = new AuraUserChat();
            chatModel = HttpContext.Session.GetObject<AuraUserChat>("chatModel");
            //d.Out(chatModel.ToString());
            _ = new Aura();
            Aura aura;
            if (HttpContext.Session.GetObject<Aura>("auraModel") is null)
            {
                d.Out("auraModel is null! setting new instance using chatModel!");
                aura = new Aura(chatModel);
                HttpContext.Session.SetObject("auraModel", aura); //instance
            }
            else
            {
                aura = HttpContext.Session.GetObject<Aura>("auraModel");
                d.Out("auraModel is NOT null!");
                d.Out("auraModel.logCreated -> " + aura.logCreated);
                //d.Out("auraModel.waitForReply -> " + aura.waitForReply);
            }
            //Boolean hasSpam = aura.IsSpam(msg);
            string username = HttpContext.Session.GetString("username");
            if(username is null)
            {
                username = "000";//default
            }
            else
            {
                aura.SetUsername(username);
            }
            
            //d.Out("AuraChat>>username -> " + username);
            if (msg.Equals("End") || aura.endChat)
            {
                //Boolean repeat = false;
                string prev_msg1 = chatModel.chatHistory[chatModel.chatHistory.Count - 1];
                if (prev_msg1 != ("p:" + msg))
                {
                    d.Out("AuraChat>>chatHistory::Added -> " + msg);
                    chatModel.chatHistory.Add("p:" + msg);//person
                }
                HttpContext.Session.SetObject("auraModel", null); //instance to null
                aura.AuraEndProcess(chatModel);//passing due to instance being null sometimes
                return View("~/Views/Home/HotelHome.cshtml");
            }
            //Boolean repeat = false;
            string prev_msg = "";
            if (aura.waitForReply)
            {
                prev_msg = chatModel.chatHistory[chatModel.chatHistory.Count - 2];
            }
            else
            {
               prev_msg = chatModel.chatHistory[chatModel.chatHistory.Count - 1];
            }
            d.Out("AuraChat>>" + prev_msg);
            if (!prev_msg.Contains(msg))
            {
                d.Out("AuraChat>>chatHistory::Added -> " + msg);
                chatModel.chatHistory.Add("p:" + msg);//person
            }
            //if don't wait for reply
            if (!aura.waitForReply)
            {
                d.Out("Aura>>Processing msg!");
                if (aura.GenerateReply(msg) == "xf001")
                {
                    aura.SetAsForwarded();
                    chatModel.chatHistory.Add("r:" + "Hold on a moment!");
                    aura.AuraSaveChatLog(chatModel);
                    ViewBag.setToRefresh = "<meta http-equiv=\"refresh\" content=\"30\"/>";
                    d.Out("AuraChat>>catched xf001 => setting forward to TRUE!");
                }
                else
                {
                    d.Out("AuraChat>>Aura Processing..!");
                    chatModel.chatHistory.Add("r:" + aura.GenerateReply(msg));
                }
            }
            else
            {
                //check if csm handled the msg
                while (aura.waitForReply)
                {
                    //timeout and forwarded is handled   
                    if (aura.ForwardedIsHandled())
                    {
                        d.Out("AuraChat>>Forward is handled by CSM!");
                        chatModel.chatHistory.Add(aura.response);
                        aura.waitForReply = false;
                    }
                    else if ((DateTime.Now - aura.activated_time).Minutes > 1 || (DateTime.Now - aura.activated_time).Hours > 0)
                    {
                        //not handled within 1min
                        //d.Out("AuraChat>>Timeout waiting for reply!");
                        chatModel.chatHistory.Add("r:" + "Leave us your email, will try to reach back to you soon!");
                        aura.waitForReply = false;
                        aura.endChat = true;
                    }
                    //d.Out("AuraChat>>waitForReply -> " + aura.waitForReply);
                    //else wait for reply
                }
                ViewBag.setToRefresh = "<meta http-equiv=\"\" content=\"\"/>";
            } 
            HttpContext.Session.SetObject("auraModel", aura);
            HttpContext.Session.SetObject("chatModel", chatModel);
            ViewBag.Collection = chatModel.chatHistory;
            return View("~/Views/Aura/Chat.cshtml");
        }

        [HttpPost]
        [HttpGet]
        public IActionResult GetFAQPage()
        {
            FAQ_dataView faq = new FAQ_dataView();
            return View("~/Views/Aura/faq_page.cshtml",faq);
        }
    }
    
    public static class SessionExtensions
    {
        public static void SetObject(this ISession session, string key, object value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }

        public static T GetObject<T>(this ISession session, string key)
        {
            var value = session.GetString(key);
            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }
    }
}
