﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MyLodge.proj1.Areas.Admin.Models;
using MyLodge.proj1.Models;
using MyLodge.proj1.Utils;
using Newtonsoft.Json;
using Raven.Client.Documents;
using Raven.Client.Documents.Session;

namespace MyLodge.proj1.Controllers
{
    public class HomeController : Controller
    {
        
    [HttpGet]
    [HttpPost, Route("GetTable2Excel")]
    public FileContentResult GetTable2Excel()
    {
        Roomreservation faq = new Roomreservation();
        List<Roomreservation> faqs = new List<Roomreservation>();
        faqs = faq.GetAllRoomReservations();
        string csv = String.Join(",", faqs.Select(x => x.ToString()).ToArray());
        return File(new System.Text.UTF8Encoding().GetBytes(csv), "text/csv", "FAQ_List.csv");
    }

        SysDiagDebug debug = new SysDiagDebug();
        private readonly ILogger<HomeController> _logger;


        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Viewww()
        {
            return View("~/Views/Reservation/reserve.cshtml");
        }
        public IActionResult Privacy()
        {
            return View();
        }
        public IActionResult HotelHome()
        {
            return View("~/Views/Home/HotelHome.cshtml");
        }

        public IActionResult LogoutUser()
        {
            HttpContext.Session.SetObject("customer", null);
            ViewBag.logbtn = "Login";
            ViewBag.log = "LoginUser";
            return View("~/Views/Reservation/homepage.cshtml");
        }

        public IActionResult UserProfile()
        {
            CustomerView cv = new CustomerView();
            var customer = HttpContext.Session.GetObject<Customer>("customer");

            if (customer is null) {
                ViewBag.logbtn = "Login";
                ViewBag.log = "LoginUser";
                return View("~/Views/Customer/UserLogin.cshtml");

            }
            Roomreservation r = new Roomreservation();
            ArrayList ar = new ArrayList();
            foreach (var res in r.GetAllRoomReservationsByEmail(customer.Email_id))
            {
                ar.Add(res.reservation_id);
                ar.Add(res.type);
                ar.Add(res.check_in);
                ar.Add(res.check_out);
                ar.Add(res.days);
                ar.Add(res.rooms);
                ar.Add(res.pay_type);
            }
            ViewBag.collection = ar;
            ViewBag.logbtn = "Logout";
            ViewBag.log = "LogoutUser";

            cv = customer.SetAlternateCast<CustomerView>(cv, customer.Cusid);
            return View("~/Views/Customer/UserProfile.cshtml", cv);
        }

        [HttpGet]

        public IActionResult Signup()
        {
            var customer = HttpContext.Session.GetObject<Customer>("customer");
            if (customer is null)
            {
                ViewBag.logbtn = "Login";
                ViewBag.log = "LoginUser";


            }
            else
            {
                ViewBag.logbtn = "Logout";
                ViewBag.log = "LogoutUser";

            }
            return View("~/Views/Customer/UserSignup.cshtml");
        }
        public IActionResult LoginUser()
        {
            return View("~/Views/Customer/UserLogin.cshtml");
        }
        public IActionResult Home()
        {
            //HttpContext.Session.SetObject("customer", null);
            var customer = HttpContext.Session.GetObject<Customer>("customer");
            if (customer is null)
            {
                ViewBag.logbtn = "Login";
                ViewBag.log = "LoginUser";
            }
            else
            {
                ViewBag.logbtn = "Logout";
                ViewBag.log = "LogoutUser";
            }
            return View("~/Views/Reservation/homepage.cshtml");
        }

        public IActionResult success()
        {

            return View("~/Views/Reservation/Sucessful.cshtml");
        }


        public IActionResult AboutUs()
        {
            var customer = HttpContext.Session.GetObject<Customer>("customer");
            if (customer is null)
            {
                ViewBag.logbtn = "Login";
                ViewBag.log = "LoginUser";
            }
            else
            {
                ViewBag.logbtn = "Logout";
                ViewBag.log = "LogoutUser";
            }
            return View("~/Views/Reservation/AboutUs.cshtml");
        }
        [HttpGet]
        public IActionResult ContactUs()
        {
            return View("~/Views/Reservation/ContactUs.cshtml");
        }

        [HttpPost]
        [HttpGet]
        public IActionResult Hall1()
        {
            var customer = HttpContext.Session.GetObject<Customer>("customer");
            if (customer is null)
            {
                ViewBag.logbtn = "Login";
                ViewBag.log = "LoginUser";
                ViewBag.name = "";
                ViewBag.email = "";
                ViewBag.setreadonly = "";
                return View("~/Views/Customer/UserLogin.cshtml");
            }
            else
            {
                ViewBag.logbtn = "Logout";
                ViewBag.log = "LogoutUser";
                ViewBag.name = customer.Fullname;
                ViewBag.setreadonly = "readonly";
                ViewBag.email = customer.Email_id;
            }
            return View("~/Views/Reservation/Halltulip.cshtml");
        }

        public IActionResult Hall2()
        {
            var customer = HttpContext.Session.GetObject<Customer>("customer");
            if (customer is null)
            {
                ViewBag.logbtn = "Login";
                ViewBag.log = "LoginUser";
                ViewBag.name = "";
                ViewBag.email = "";
                ViewBag.setreadonly = "";
                return View("~/Views/Customer/UserLogin.cshtml");
            }
            else
            {
                ViewBag.logbtn = "Logout";
                ViewBag.log = "LogoutUser";
                ViewBag.name = customer.Fullname;
                ViewBag.setreadonly = "readonly";
                ViewBag.email = customer.Email_id;
            }
            return View("~/Views/Reservation/Hallorchid.cshtml");
        }
        [HttpPost]
        [HttpGet]
        public IActionResult Hall3()
        {
            var customer = HttpContext.Session.GetObject<Customer>("customer");
            if (customer is null)
            {
                ViewBag.logbtn = "Login";
                ViewBag.log = "LoginUser";
                ViewBag.name = "";
                ViewBag.email = "";
                ViewBag.setreadonly = "";
                return View("~/Views/Customer/UserLogin.cshtml");
            }
            else
            {
                ViewBag.logbtn = "Logout";
                ViewBag.log = "LogoutUser";
                ViewBag.setreadonly = "readonly";
                ViewBag.name = customer.Fullname;
                ViewBag.email = customer.Email_id;
            }
            return View("~/Views/Reservation/Halllotus.cshtml");
        }

        public IActionResult Room1()
        {
            var customer = HttpContext.Session.GetObject<Customer>("customer");
            if (customer is null)
            {
                ViewBag.logbtn = "Login";
                ViewBag.log = "LoginUser";
                ViewBag.name = "";
                ViewBag.email = "";
                ViewBag.setreadonly = "";
                return View("~/Views/Customer/UserLogin.cshtml");

            }
            else {
                ViewBag.logbtn = "Logout";
                ViewBag.log = "LogoutUser";
                ViewBag.setreadonly = "readonly";
                ViewBag.name = customer.Fullname;
                ViewBag.email = customer.Email_id;
            }
            HttpContext.Session.SetString("rtype", "Standard");
            return View("~/Views/Reservation/standardroom.cshtml");
        }


        public IActionResult Room2()
        {
            var customer = HttpContext.Session.GetObject<Customer>("customer");
            if (customer is null)
            {
                ViewBag.logbtn = "Login";
                ViewBag.log = "LoginUser";
                ViewBag.name = "";
                ViewBag.email = "";
                ViewBag.setreadonly = "";
                return View("~/Views/Customer/UserLogin.cshtml");

            }
            else
            {
                ViewBag.logbtn = "Logout";
                ViewBag.log = "LogoutUser";
                ViewBag.setreadonly = "readonly";
                ViewBag.name = customer.Fullname;
                ViewBag.email = customer.Email_id;
            }

            HttpContext.Session.SetString("rtype", "Superior");
            return View("~/Views/Reservation/superiorroom.cshtml");
        }

        public IActionResult Room3()
        {
            var customer = HttpContext.Session.GetObject<Customer>("customer");
            if (customer is null)
            {
                ViewBag.logbtn = "Login";
                ViewBag.log = "LoginUser";
                ViewBag.name = "";
                ViewBag.email = "";
                ViewBag.setreadonly = "";
                return View("~/Views/Customer/UserLogin.cshtml");

            }
            else
            {
                ViewBag.logbtn = "Logout";
                ViewBag.log = "LogoutUser";
                ViewBag.setreadonly = "readonly";
                ViewBag.name = customer.Fullname;
                ViewBag.email = customer.Email_id;
            }

            HttpContext.Session.SetString("rtype", "Super Deluxe");
            return View("~/Views/Reservation/superdeluxe.cshtml");
        }

        [HttpGet]
        [HttpPost]
        public IActionResult gallery1()
        {
            var customer = HttpContext.Session.GetObject<Customer>("customer");
            if (customer is null)
            {
                ViewBag.logbtn = "Login";
                ViewBag.log = "LoginUser";
            }
            else
            {
                ViewBag.logbtn = "Logout";
                ViewBag.log = "LogoutUser";
            }
            return View("~/Views/Reservation/Sucessful.cshtml");
        }

        public IActionResult Hallview()
        {
            var customer = HttpContext.Session.GetObject<Customer>("customer");
            if (customer is null)
            {
                ViewBag.logbtn = "Login";
                ViewBag.log = "LoginUser";
            }
            else
            {
                ViewBag.logbtn = "Logout";
                ViewBag.log = "LogoutUser";
            }
            HallReservationView hrv = new HallReservationView();
            return View("~/Views/Reservation/Reservetable.cshtml", hrv);
        }

        public IActionResult Roomview()
        {
            var customer = HttpContext.Session.GetObject<Customer>("customer");
            if (customer is null)
            {
                ViewBag.logbtn = "Login";
                ViewBag.log = "LoginUser";
            }
            else
            {
                ViewBag.logbtn = "Logout";
                ViewBag.log = "LogoutUser";
            }
            RoomReservationView hrv = new RoomReservationView();
            return View("~/Views/Reservation/ReservetableRoom.cshtml", hrv);
        }

        [HttpPost]
        public IActionResult Route(string submitButton)
        {
            if (submitButton == "View members")
            {
                CustomerView hrv = new CustomerView();
                //return RedirectToAction("Index", "Home", new { area = "Admin" });
                return View("~/Areas/Admin/Views/Customer/AdminCustomerMgt.cshtml", hrv);
                // return View("~/Views/Home/HotelHome.cshtml");
            }
            else if(submitButton == "Admin"){
                return RedirectToAction("Index", "Home", new { area = "Admin" });
            }
            else if(submitButton == "Chat"){
                return RedirectToAction("Chat", "AuraChatFaq");
            }
            else if (submitButton == "View room reservation")
            {
                RoomReservationView hrv = new RoomReservationView();
                return View("~/Areas/Admin/Views/Reservation/ReservetableRoom.cshtml", hrv);
            }
            else if (submitButton == "View contactus")
            {
                ContactUsView hrv = new ContactUsView();
                return View("~/Areas/Admin/Views/Customer/AdminContactus.cshtml", hrv);
            }
            else if (submitButton == "View hall reservation")
            {
                HallReservationView hrv = new HallReservationView();
                return View("~/Areas/Admin/Views/Reservation/Reservetable.cshtml", hrv);
            }
            else
            {
                var customer = HttpContext.Session.GetObject<Customer>("customer");
                if (customer is null)
                {
                    ViewBag.logbtn = "Login";
                    ViewBag.log = "LoginUser";
                }
                else
                {
                    ViewBag.logbtn = "Logout";
                    ViewBag.log = "LogoutUser";
                }
                return View("~/Views/Reservation/homepage.cshtml");
            }
        }



        //Reservation Controller
        public IActionResult room(string submitButton)
        {
            //debug.Out("btn1 = " + submitButton);
            ViewBag.btnPress = "Button Pressed: " + submitButton;
            if (submitButton == "Admin")
            {
                return RedirectToAction("Index", "Home", new { area = "Admin" });
            }
            else
            {
                return View("~/Views/Reservation/superiorroom.cshtml");
            }
        }
        public IActionResult submit(string submitButton)
        {
            //debug.Out("btn1 = " + submitButton);
            ViewBag.btnPress = "Button Pressed: " + submitButton;
            if (submitButton == "Admin")
            {
                return RedirectToAction("Index", "Home", new { area = "Admin" });
            }
            else
            {
                return View("~/Views/Reservation/room.cshtml");
            }
        }
        [HttpPost]
        public IActionResult Reserve2(Hallreservation hr, string submitButton)
        {
            //debug.Out("btn1 = " + submitButton);
            hr.type = HttpContext.Session.GetString("rtype");

            if (hr.pay_type == "Online payment")
            {
                hr.Save();
                HallReservationView hrv = new HallReservationView();
                //return RedirectToAction("Index", "Home", new { area = "Admin" });
                return View("~/Views/Reservation/Payment.cshtml", hrv);
            }
            else
            {
                hr.Save();
                HallReservationView hrv = new HallReservationView();
                return View("~/Views/Reservation/Sucessful.cshtml");

            }

        }

        [HttpPost]
        public IActionResult Reserve1(Roomreservation hr)
        {

            //  debug.Out("btn1 = " + submitButton);
            //     ViewBag.btnPress = "Button Pressed: " + submitButton;
            hr.type = HttpContext.Session.GetString("rtype");

            if (hr.pay_type == "Online payment")
            {
                hr.Save();
                HallReservationView hrv = new HallReservationView();
                //return RedirectToAction("Index", "Home", new { area = "Admin" });
                return View("~/Views/Reservation/Payment.cshtml", hrv);
            }
            else
            {
                hr.Save();
                HallReservationView hrv = new HallReservationView();
                return View("~/Views/Reservation/Sucessful.cshtml");

            }

        }
        [HttpGet]
        [HttpPost]
        public FileContentResult GetRevTableExcel()
        {
            Roomreservation c = new Roomreservation();
            List<Roomreservation> cus = new List<Roomreservation>();
            cus = c.GetAllRoomReservations();
            string csv = String.Join(",", cus.Select(x => x.ToString()).ToArray());
            return File(new System.Text.UTF8Encoding().GetBytes(csv), "text/csv", "ReservationList.csv");
        }

        [HttpPost]
        public IActionResult EditRoom(String id, Roomreservation sr)
        {
            sr.type = HttpContext.Session.GetString("rtype");

            debug.Out("id = " + id);
            HttpContext.Session.SetInt32("reservation_id", Int32.Parse(id));
            Roomreservation r = new Roomreservation();
            r = r.GetRoomReservationByreservation_id(Int32.Parse(id));
            RoomReservationView hrv = new RoomReservationView();
            hrv = r.SetAlternateCast<RoomReservationView>(hrv, Int32.Parse(id));
            if (r.type == "Standard")
            {
                return View("~/Views/Reservation/UpdateRoom.cshtml", hrv);
            }
            else
            if (r.type == "Superior")
            {
                return View("~/Views/Reservation/UpdateRoom2.cshtml", hrv);
            }
            else
            {
                return View("~/Views/Reservation/UpdateRoom3.cshtml", hrv);
            }
        }
        [HttpPost]
        public IActionResult EditRoom2(String id)
        {
            debug.Out("id = " + id);
            HttpContext.Session.SetInt32("reservation_id", Int32.Parse(id));
            Roomreservation r = new Roomreservation();
            r = r.GetRoomReservationByreservation_id(Int32.Parse(id));
            RoomReservationView hrv = new RoomReservationView();
            hrv = r.SetAlternateCast<RoomReservationView>(hrv, Int32.Parse(id));
            return View("~/Views/Reservation/UpdateRoom.cshtml", hrv);
        }
        [HttpPost]
        public IActionResult CancelRoom(String id)
        {
            debug.Out("id = " + id);

            Roomreservation r = new Roomreservation();
            var customer = new Customer();
            customer = HttpContext.Session.GetObject<Customer>("customer");

            r.Delete(Int32.Parse(id));
            CustomerView cv = new CustomerView();

            cv = customer.SetAlternateCast<CustomerView>(cv, customer.Cusid);

            ArrayList ar = new ArrayList();
            foreach (var res in r.GetAllRoomReservationsByEmail(customer.Email_id))
            {
                ar.Add(res.reservation_id);
                ar.Add(res.type);
                ar.Add(res.check_in);
                ar.Add(res.check_out);
                ar.Add(res.days);
                ar.Add(res.rooms);
                ar.Add(res.pay_type);
            }
            ViewBag.collection = ar;
            return View("~/Views/Customer/UserProfile.cshtml", cv);
        }

        [HttpPost]
        [HttpGet]
        public IActionResult HallReserve()
        {
            var customer = new Customer();
            customer = HttpContext.Session.GetObject<Customer>("customer");
            if (!(customer is null))
            {
                ViewBag.login = "Logout";
            }
            else
            {
                ViewBag.login = "Login";
            }
            return View("~/Views/Reservation/Hallshomepage.cshtml");
        }
        [HttpPost]
        [HttpGet]
        public IActionResult RoomReserve()
        {
            var customer = new Customer();
            customer = HttpContext.Session.GetObject<Customer>("customer");
            if (!(customer is null))
            {
                ViewBag.login = "Logout";
            }
            else
            {
                ViewBag.login = "Login";
            }
            return View("~/Views/Reservation/Roomshomepage.cshtml");
        }

        [HttpPost]
        public IActionResult UpdateRoom(Roomreservation r)
        {

            var cus = new Customer();
            cus = HttpContext.Session.GetObject<Customer>("customer");
            r.reservation_id = HttpContext.Session.GetInt32("reservation_id").Value;
            debug.Out("reservation_id=" + r.reservation_id);
            r.UpdateRoom();

            //get reservation table
            CustomerView cv = new CustomerView();
            cv = cus.SetAlternateCast<CustomerView>(cv, cus.Cusid);
            ArrayList ar = new ArrayList();
            foreach (var res in r.GetAllRoomReservationsByEmail(cus.Email_id))
            {
                ar.Add(res.reservation_id);
                ar.Add(res.type);
                ar.Add(res.check_in);
                ar.Add(res.check_out);
                ar.Add(res.days);
                ar.Add(res.rooms);
                ar.Add(res.pay_type);
            }
            ViewBag.collection = ar;
            return View("~/Views/Customer/UserProfile.cshtml", cv);
            //

        }

        [HttpPost]
        public IActionResult SearchRoomReserve(String reservation_id)
        {
            SysDiagDebug d = new SysDiagDebug();
            d.Out("reservation_id=" + reservation_id);
            Roomreservation r = new Roomreservation();
            RoomReservationView cv = new RoomReservationView();
            r = r.GetRoomReservationByreservation_id(Int32.Parse(reservation_id));
            var c = HttpContext.Session.GetObject<Customer>("customer");
            cv.reservation_id = r.reservation_id;
            cv.name = r.name;
            cv.email = r.email;
            cv.check_in = r.check_in;
            cv.check_out = r.check_out;
            cv.type = r.type;
            cv.days = r.days;
            cv.rooms = r.rooms;
            cv.pay_type = r.pay_type;
            if (r is null)
            {
                ViewBag.error = "Reservation ID not found!";
                return View("~/Areas/Admin/Views/Reservation/ReservetableRoom.cshtml");
            }
            else
            {

                return View("~/Areas/Admin/Views/Reservation/ReservetableRoom.cshtml", cv);
            }
        }

        private static Hallreservation ReadFromRavenR(IDocumentStore store, string id)
        {
            using (IDocumentSession session = store.OpenSession())
            {
                return session.Load<Hallreservation>("Hallreservation/" + id); //get Employee using id "Employee/id"
            }
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]

        [HttpPost]
        public IActionResult GetReservationTable()
        {
            HallReservationView hrv = new HallReservationView();
            return View("~/Views/Reservation/Reservetable.cshtml", hrv); //pass inherited class obj
        }




        //ContactUs Controller
        [HttpPost]
        public IActionResult GetContactTable()
        {
            ContactUsView cus = new ContactUsView();
            return View("~/Views/Reservation/AdminContactus.cshtml", cus); //pass inherited class obj
        }

        [HttpPost]
        public IActionResult Contact(ContactUs c)
        {
            c.Save();
            ContactUsView cv = new ContactUsView();
            return View("~/Views/Reservation/homepage.cshtml", cv);
        }




        //Customer Controller
        [HttpPost]
        public IActionResult DeleteCustomer(Customer c)
        {

            var cus = new Customer();
            cus = HttpContext.Session.GetObject<Customer>("customer");
            c.Cusid = cus.Cusid;
            c.Delete(c.Cusid);
            return View("~/Views/Reservation/homepage.cshtml");
        }


        private static Customer ReadFromRaven(IDocumentStore store, string id)
        {
            using (IDocumentSession session = store.OpenSession())
            {
                return session.Load<Customer>("Customer/" + id); //get Customer using id "Customer/id"
            }
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]

        [HttpPost]
        public IActionResult GetCustomerTable()
        {
            CustomerView cus = new CustomerView();
            return View("~/Views/Customer/AdminCustomerMgt.cshtml", cus); //pass inherited class obj
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


        [HttpPost]
        public IActionResult Customer(Customer hr)
        {
            hr.Save();

            CustomerView cv = new CustomerView();
            cv = hr.SetAlternateCast<CustomerView>(cv, hr.Cusid);//added
            HttpContext.Session.SetObject("customer", hr);//added
            ArrayList ar = new ArrayList();
            ViewBag.collection = ar;


            return View("~/Views/Customer/UserProfile.cshtml", cv);
        }

        [HttpPost, Route("Login")]
        public IActionResult Login(Customer c)
        {
            //Customer customer = new Customer();
            var customer = new Customer();//changed class to var
            customer = c.GetCustomerByUserId(c.UserID);
            CustomerView cv = new CustomerView();

            if (customer is null)
            {
                ViewBag.error = "UserID not found!";
                return View("~/Views/Customer/UserLogin.cshtml");
            }
            else
            {
                if (!customer.PasswordIsMatching(c.Password))
                {
                    ViewBag.error = "Password is incorrect!";
                    return View("~/Views/Customer/UserLogin.cshtml");
                }
                Roomreservation r = new Roomreservation();

                cv = customer.SetAlternateCast<CustomerView>(cv, customer.Cusid);
                HttpContext.Session.SetObject("customer", customer);

                ArrayList ar = new ArrayList();
                foreach (var res in r.GetAllRoomReservationsByEmail(customer.Email_id))
                { ar.Add(res.reservation_id);
                    ar.Add(res.type);
                    ar.Add(res.check_in);
                    ar.Add(res.check_out);
                    ar.Add(res.days);
                    ar.Add(res.rooms);
                    ar.Add(res.pay_type);
                }
                ViewBag.collection = ar;
                return View("~/Views/Customer/UserProfile.cshtml", cv);
            }
        }

        [HttpPost]
        public IActionResult UpdateCustomer(Customer c)
        {
            Roomreservation r = new Roomreservation();
            var cus = new Customer();
            cus = HttpContext.Session.GetObject<Customer>("customer");
            c.Cusid = cus.Cusid;
            c.UpdateCustomer();

            //get reservation table
            CustomerView cv = new CustomerView();
            c = c.Read(c.Cusid);
            cv = c.SetAlternateCast<CustomerView>(cv, c.Cusid);
            HttpContext.Session.SetObject("customer", c);
            ArrayList ar = new ArrayList();
            foreach (var res in r.GetAllRoomReservationsByEmail(cus.Email_id))
            {
                ar.Add(res.reservation_id);
                ar.Add(res.type);
                ar.Add(res.check_in);
                ar.Add(res.check_out);
                ar.Add(res.days);
                ar.Add(res.rooms);
                ar.Add(res.pay_type);
            }
            ViewBag.collection = ar;
            return View("~/Views/Customer/UserProfile.cshtml", cv);
        }


        [HttpPost]
        public IActionResult Search(String Email_id)
        {
            SysDiagDebug d = new SysDiagDebug();
            d.Out("Email_id=" + Email_id);
            Customer customer = new Customer();
            CustomerView cv = new CustomerView();
            customer = customer.GetCustomerByEmail(Email_id);

            cv.Cusid = customer.Cusid;
            cv.Fullname = customer.Fullname;
            cv.DateofBirth = customer.DateofBirth;
            cv.ContactNo = customer.ContactNo;
            cv.Email_id = customer.Email_id;
            cv.Nic = customer.Nic;
            cv.Password = customer.Password;
            cv.UserID = customer.UserID;
            cv.Address = customer.Address;
            cv.Country = customer.Country;
            if (customer is null)
            {
                ViewBag.error = "Customer ID not found!";
                return View("~/Areas/Admin/Views/Customer/AdminCustomerMgt.cshtml");
            }
            else
            {

                return View("~/Areas/Admin/Views/Customer/AdminCustomerMgt.cshtml", cv);
            }
        }

        [HttpGet]
        [HttpPost]
        public FileContentResult GetCusTableExcel()
        {
            Customer c = new Customer();
            List<Customer> cus = new List<Customer>();
            cus = c.GetAllMemberdetails();
            string csv = String.Join(",", cus.Select(x => x.ToString()).ToArray());
            return File(new System.Text.UTF8Encoding().GetBytes(csv), "text/csv", "Customer_List.csv");
        }

    }

 
public class CustomerView : Customer
    {
        public CustomerView() : base("Customer")
        {

        }
    }

    public class HallReservationView : Hallreservation
    {
        public HallReservationView() : base("Hallreservation")
        {

        }
    }

    public class RoomReservationView : Roomreservation
    {
        public RoomReservationView() : base("Roomreservation")
        {

        }
    }

    public class ContactUsView : ContactUs
    {
        public ContactUsView() : base("ContactUs")
        {

        }

    }
}


   





