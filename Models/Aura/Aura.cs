﻿using MyLodge.proj1.Areas.Admin.Controllers;
using MyLodge.proj1.Areas.Admin.Models.CSM;
using MyLodge.proj1.Areas.Admin.Models;
using MyLodge.proj1.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Globalization;
using System.Text.RegularExpressions;

namespace MyLodge.proj1.Models.Aura
{
    class AuraModel
    {
        public string[] Names { get; set; }
        public string[] Greet { get; set; }
        public AuraModel()
        {

        }
        
    }
    public class Aura
    {
        UseSharpNlp nlp = new UseSharpNlp();//open nlp port
        SysDiagDebug debug = new SysDiagDebug("Aura>>");//for debug
        FAQ_model faq = new FAQ_model(); //faq
        public static string Name = "Aura";
        AuraStats auraStats = new AuraStats();//for csm manager
        AuraLogs chatlog = new AuraLogs();//chat log
        AuraUserChat chat;

        bool hasSpam;
        bool forwarded;
        bool regCus;
        public bool waitForReply = false;
        public DateTime activated_time;
        string username;
        public bool endChat = false;
        public bool reset_forwarded = false;
        public bool logCreated = false;
        public Aura()
        {
            //for faq keyword processing
        }
        //when adding new FAQs
        public List<string> GetFaqKeywordsFromAura(string faq)
        {
            string[] sent_words = nlp.GetTokens(faq);
            string[] word_tags = nlp.GetTags(faq);
            List<string> keywords = new List<string>();
            List<string> nouns = nlp.GetSentenceNouns(sent_words,word_tags);
            List<string> verbs = nlp.GetSentenceVerbs(sent_words,word_tags);
            keywords.AddRange(nouns);
            keywords.AddRange(verbs);
            return keywords;
        }
        public Aura(AuraUserChat obj)
        {
            chat = obj;//passed from controller
            //default
            hasSpam = false;
            forwarded = false;
            regCus = false;
            username = "000";
            //chatlog = new AuraLogs();
            chatlog.DeleteOldLogs();
            
            auraStats = auraStats.Read();
            auraStats.Inquiries_incoming++;
            auraStats.Inquiries_handled++;
            auraStats.Total_received_today++;
            auraStats.Update();
        }
        /*
         * AuraEndProcess called at the end of chat
         */
        public void AuraEndProcess(AuraUserChat obj)
        {
            auraStats = auraStats.Read();
            auraStats.Inquiries_incoming--;
            auraStats.Inquiries_handled--;
            auraStats.Update();
            //end
            if (!logCreated)
            {
                chatlog.CreateChatLog(hasSpam, forwarded, regCus, username, obj);
                logCreated = true;
            }
            else
            {
                chatlog.Forwarded = forwarded;
                chatlog.UpdateChatLogs();
            }
        }
        public void AuraSaveChatLog(AuraUserChat obj)
        {
            if (!logCreated)
            {
                debug.Out("Creating new AuraLog!");
                chatlog.CreateChatLog(hasSpam, forwarded, regCus, username, obj);
                logCreated = true;
            }
            else
            {

                debug.Out("Updating AuraLog! using id = "  +chatlog.Chat_id);
                chatlog.Forwarded = forwarded;
                chatlog.UpdateSelf(obj);
            }   
        }
        
        public Boolean ForwardedIsHandled()
        {
            
            response = "";
            chat = new AuraUserChat();
            int id = chatlog.Chat_id;
            //debug.Out("Aura>>reloading chatlog with id = " + id);
            chatlog = chatlog.ReadChatLogs(id);//reload same record
            //reponse = fullchat[fullchat.Count - 1];
            //debug.Out("Aura>>IsForwardedHandled? -> " + !chatlog.Forwarded);
            if (chatlog.Forwarded==false && waitForReply==true)
            {
                try
                {
                    chat = chat.GetUserChat(id);//load chat record
                    string prev_response_predict = chat.chatHistory[chat.chatHistory.Count - 3];
                    response = chat.chatHistory[chat.chatHistory.Count - 1];
                    
                    reset_forwarded = true;
                }
                catch (NullReferenceException)
                {
                    debug.Out("NO chat with id = " + id);
                    this.response = "Sorry!";
                    reset_forwarded = true;
                }
                //aurastats
                return true;
            }
            return false;
        }
        public Boolean IsSpam(string sentence)
        {
            //if true
            //chatlog.Has_spam = true;
            hasSpam = true;
            //else
            return false;
        }
        public void SetUsername(string uname)
        {
            regCus = true;
            this.username = uname;
        }
        public void SetAsForwarded()
        {
            debug.Out("SetAsForwarded()");
            activated_time = DateTime.Now;
            forwarded = true;
            reset_forwarded = false;
            waitForReply = true;
            //aurastats
            auraStats = auraStats.Read();
            auraStats.Inquiries_forwarded++;
            auraStats.Update();
        }

        /*
         * ============== Generate Reply ===========================
         */
        Sentence_breakdown sentence;
        public string GenerateReply(string inputData)
        {
            debug.Out("inputData -> " + inputData);
            string[] sentence_tags = nlp.GetTags(inputData);
            string[] sentence_words = nlp.GetTokens(inputData);
            string nerModel = nlp.NERTag(inputData); //get Named entities
            string[] greet = {"lol","uhh","hello"};
            List<string> nouns = nlp.GetSentenceNouns(sentence_words, sentence_tags);
            List<string> verbs = nlp.GetSentenceVerbs(sentence_words, sentence_tags);
            List<string> cd = nlp.GetSentenceCardinalNumbers(sentence_words, sentence_tags);
            new Sentence_breakdown(nouns,verbs,cd);

            string sp = SentenceProcessingAccount(sentence_words, sentence_tags,inputData);
            if (inputData.ToLower().Contains("thank") || inputData.ToLower().Contains("gracias"))
            {
                endChat = true;
                return "Welcome!";
            }
            if (sentence_words.Length <= 3) //3 word sentence
            {
                Random random = new Random();
                int randomNumber = random.Next(0, 2);
                for (int i = 0; i < sentence_tags.Length; i++)
                {
                    if (sentence_tags[i] == "UH") //Interjection
                    {
                        //return greet[randomNumber];
                        return "uhh";
                    }
                }
                return inputData;
            }
            else if (GetMathingFaqIdUsingKeywords(nouns,verbs) != -1)
            {
                debug.Out("Fetched using Keyword list!!");
                return faq.GetAnswerById(GetMathingFaqIdUsingKeywords(nouns, verbs));
            }else if(sp != "nAccount-related")
            {
                return sp;
            }
            /*
            else if(GetMatchingFaqId(inputData) != -1){
                debug.Out("Fetched using FAQ processing!!");
                return faq.GetAnswerById(GetMatchingFaqId(inputData));
            }*/
            else
            {
                debug.Out("Message cannot be processed, insufficient data, Forwarding to CSM!");
                return "xf001"; //msg to forward current chat to CSM
            }
        }
        public string processNER(string ner_converted)
        {
            foreach(string model in UseSharpNlp.NERmodels)
            {

            }
            return null;
        }
        /*
         * ======= Get Matching FAQ ID using keywords from scratch ====================
         * process each faq by extracting nouns and verbs
         */
        public int GetMatchingFaqId(String sentence)
        {
            string[] sentence_tags = nlp.GetTags(sentence);
            string[] sentence_words = nlp.GetTokens(sentence);
            string nerModel = nlp.NERTag(sentence); //get Named entities
            string[] greet = { "lol", "uhh", "hello" };
            List<string> nouns = nlp.GetSentenceNouns(sentence_words, sentence_tags);
            List<string> verbs = nlp.GetSentenceVerbs(sentence_words, sentence_tags);

            int nCount = nouns.Count;
            int vCount = verbs.Count;

            //int sentence_len = sentence_words.Length, matches = 0;
            int index = 0;
            foreach(string faq_question in faq.GetQuestions())
            {
                string[] faq_words = nlp.GetTokens(faq_question);
                string[] faq_tags = nlp.GetTags(faq_question);
                List<string> faq_nouns = nlp.GetSentenceNouns(faq_words, faq_tags);
                List<string> faq_verbs = nlp.GetSentenceVerbs(faq_words, faq_tags);
                int faq_n = 0;
                int faq_v = 0;

                for(int i = 0; i < nCount; i++)
                {
                    if (faq_nouns[i].ToLower() == nouns[i].ToLower())
                    {
                        faq_n++;
                    }
                    if (faq_verbs[i].ToLower() == verbs[i].ToLower())
                    {
                        faq_v++;
                    }
                }
                if(faq_n==nCount && faq_v == vCount)
                {
                    return index;
                }
                index++;
            }
            return -1;
        }
        /*
         * ==========Get Matching FAQ ID using keyword recog ===================
         */
        public int GetMathingFaqIdUsingKeywords(List<string> sentence_nouns, List<string> sentence_verbs)
        {
            int index = 0;
            int matches = 0;
            List<string> sent = new List<string>();
            sent.AddRange(sentence_nouns);
            sent.AddRange(sentence_verbs);
            List<List<string>> faq_keywords = faq.GetKeywords();
            foreach (List<string> keywords in faq_keywords) { 
                foreach(string keyword in keywords)
                {
                    foreach(string word in sent)
                    {
                        debug.Out("Keywordmatch::word -> " + Word2Base(word));
                        debug.Out("Keywordmatch::keyword -> " + Word2Base(keyword));
                        if(Word2Base(word) == Word2Base(keyword))
                        {
                            matches++;
                        }
                        if(word == "check" || word == "verify")
                        {
                            matches++;
                        }
                    }
                }
                if(matches == sent.Count)
                {
                    return index;
                }
                index++;
            }
            return -1;
        } 
        /*
         * Check if user is requesting to update account password
         */
        public string SentenceProcessingAccount(string[] tokens, string[] tags, string msg)
        {
            bool account = false;
            foreach(string token in tokens)
            {
                if(token.ToLower() == "nic")
                {
                    //account mgmt
                    account = true;
                }
            }
            if (account)
            {
                string nic = "",email = "";
                //get nic and email
                for(int i = 0; i < tokens.Length; i++) { 
                    if(tags[i] == "CD" || tags[i] == "JJ")
                    {
                        if (Regex.IsMatch(tokens[i], @"^\d+"))
                        {
                            nic = tokens[i];
                        }
                    }
                }
                var emailMatches = Regex.Match(msg, @"[a-zA-z0-9\.]+@[a-zA-z0-9\.]+\.[A-Za-z]+");
                if (emailMatches.Success)
                {
                    email = emailMatches.Value.ToString();
                }
                //debug.Out("Nic -> " + nic + " and email -> " + email);
                //search users
                Customer customer = new Customer("Customer");
                foreach(Customer c in customer.GetAllMemberdetails())
                {
                   if(c.Nic == nic && c.Email_id == email)
                    {
                        //reset password
                        string password = Password.Generate(8, 3);
                        //c.ResetPassword(password);
                        return "Password reset successful, new password: " + password;
                    }
                }
                /*
                employeeRegistration employee = new employeeRegistration("");
                employee = employee.GetEmployeeByNIC(nic);
                if (!(employee is null))
                {
                    if(employee.email == email)
                    {
                        //reset password
                        string password = Password.Generate(8, 3);
                        //employee.ResetPassword(password);
                        return "Password reset successful, new password: " + password;
                    }
                }*/
                return "Sorry! We will look into it and notify you soon!";
            }
            return "nAccount-related";
        }
        public string SentenceProcessing2()
        {
            Boolean food = false;
            Boolean reserve = false;
            Boolean room = false;
            Boolean hall = false;
            Boolean order = false;
            Boolean password = false;
            Boolean forgot = false;
            Boolean user = false;
            List<string> nouns = sentence.nouns;
            List<string> verbs = sentence.verbs;
            List<string> cardinalNumber = sentence.cardinalNumber;
            Cardinal_numbers number = new Cardinal_numbers(cardinalNumber);
            foreach (string n in nouns)
            {
                string np = sentence.Word2Base(n);
                if(np == "food")
                {
                    food = true;
                }
                if(np == "reserve")
                {
                    reserve = true;
                }
                if(np == "room")
                {
                    room = true;
                }
                if(np == "hall")
                {
                    hall = true;
                }
                if(np == "password")
                {
                    password = true;
                }
                if(np == "user")
                {
                    user = true;
                }
            }
            foreach (string v in verbs)
            {
                string vp = sentence.Word2Base(v);
                if (v == "order")
                {
                    order = true;
                }
                if(v=="forgot" || v == "reset")
                {
                    forgot = true;
                }
            }
            //about room
            if (room && reserve)
            {
                if (number.has_cd)
                {
                    
                }
            }
            //about hall
            if (hall && reserve)
            {

            }
            if(food && order)
            {
                //about kitchen management
                if (number.has_cd)
                {

                }
            }
            if(password && forgot)
            {
                //about account management
                if (user)
                {
                    return "Type in your NIC and email address..?";
                }
            }
            return "complex";
        }
        public string Word2Base(string word)
        {
            //reservations
            if (word.Length > 3)//is
            {
                if (word.ToLower().EndsWith("s"))
                {

                    word = word.Substring(0, word.Length - 2);
                    //debug.Out("Word2Base: plural2base -> " + word);
                }
            }
            //temporary fix
            if (word == "reservation")
            {
                return "reserve";
            }
            if (word == "confirmation")
            {
                return "confirm";
            }
            return word;
        }
        /*
        public void processCD(List<string> cardinalNumber)
        {
            double numberD = 0.00;
            int numberInt = 0;
            long s = 0;
            foreach (string cnx in cardinalNumber)
            {
                CultureInfo provider = new CultureInfo("en-US");
                //long s = long.Parse(cnx, NumberStyles.HexNumber);
                bool is_alphaNumeric = long.TryParse(cnx, NumberStyles.HexNumber, provider, out s);
                if (is_alphaNumeric)
                {
                    Console.WriteLine("number is alpha-numeric for " + s);
                    numberInt = (int)s;
                }
                if (cnx.Contains("."))
                {
                    Console.WriteLine("number is a Double for " + numberD);
                    numberD = Double.Parse(cnx);
                }
                bool is_integer = int.TryParse(cnx, out numberInt);
                if (is_integer)
                {
                    Console.WriteLine("number is alpha-numeric for " + numberInt);
                }
            }
        }*/
        //from csm
        public string response = "";
        //for processing
        private struct Cardinal_numbers
        {
            public List<string> cardinalnumbers;
            public double numberD;
            public int numberInt;
            public long s;
            public bool is_alphaNumeric;
            public bool is_integer;
            public bool is_double;
            public bool has_cd;
            public Cardinal_numbers(List<string> cardinalNumbers_list)
            {
                numberD = 0.00;
                numberInt = 0;
                s = 0;
                is_alphaNumeric = false;
                is_integer = false;
                is_double = false;
                has_cd = false;
                if (cardinalNumbers_list.Any<string>())
                {
                    cardinalnumbers = cardinalNumbers_list;
                    has_cd = true;
                }
                else
                {
                    cardinalnumbers = new List<string>();
                }
            }
            public void processCD(List<string> cardinalNumber)
            {

                foreach (string cnx in cardinalNumber)
                {
                    CultureInfo provider = new CultureInfo("en-US");
                    //long s = long.Parse(cnx, NumberStyles.HexNumber);
                    is_alphaNumeric = long.TryParse(cnx, NumberStyles.HexNumber, provider, out s);
                    if (is_alphaNumeric)
                    {
                        //Console.WriteLine("number is alpha-numeric for " + s);
                        numberInt = (int)s;
                    }
                    if (cnx.Contains("."))
                    {
                        is_double = true;
                        //Console.WriteLine("number is a Double for " + numberD);
                        try
                        {
                            numberD = Double.Parse(cnx);
                        }
                        catch (FormatException)
                        {
                            is_double = false;
                        }
                    }
                    is_integer = int.TryParse(cnx, out numberInt);
                    if (is_integer)
                    {
                        //Console.WriteLine("number is alpha-numeric for " + numberInt);
                    }
                }
            }
         }
        private struct Sentence_breakdown
        {
            public List<string> verbs;
            public List<string> nouns;
            public List<string> cardinalNumber;
            public Sentence_breakdown(List<string> nouns_list, List<string> verbs_list, List<string> cardinalNumbers_list)
            {
                
                if (nouns_list.Any<string>())
                {
                    nouns = nouns_list;
                }
                else
                {
                    nouns = new List<string>();
                }
                if (verbs_list.Any<string>())
                {
                    verbs = verbs_list;
                }
                else
                {
                    verbs = new List<string>();
                }
                if (cardinalNumbers_list.Any<string>())
                {
                    cardinalNumber = cardinalNumbers_list;
                }
                else
                {
                    cardinalNumber = new List<string>();
                }
            }
            public List<string> Word2Base(List<string> words)
            {
                List<string> processedwords = new List<string>();
                //reservations
                foreach (String word in words)
                {
                    string a = word;
                    if (word.Length > 3)//is
                    {
                        if (word.ToLower().EndsWith("s"))
                        {
                            a = word.Substring(0, word.Length - 2);
                        }
                    }
                    //temporary fix
                    if (a == "reservation")
                    {
                        processedwords.Add("reserve");
                    }
                    if (a == "confirmation")
                    {
                        processedwords.Add("confirm");
                    }
                }
                return processedwords;
            }
            public string Word2Base(string word)
            {
                String processedword = "";
                //reservations
                if (word.Length > 3)//is
                {
                    if (word.ToLower().EndsWith("s"))
                    {

                        processedword = word.Substring(0, word.Length - 2);
                        //debug.Out("Word2Base: plural2base -> " + word);
                    }
                }
                //temporary fix
                if (processedword == "reservation")
                {
                    return "reserve";
                }
                if (processedword == "confirmation")
                {
                    return "confirm";
                }
                return word;
            }
            
        }//end struct
    }//end class
}//end namespace
