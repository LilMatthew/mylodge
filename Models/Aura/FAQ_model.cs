﻿using MyLodge.proj1.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyLodge.proj1.Models.Aura
{
    public class FAQ_model
    {
        [Newtonsoft.Json.JsonIgnore]
        private readonly RavenDBConnector ravendb = RavenDBConnector.Instance();
        [Newtonsoft.Json.JsonIgnore]
        private string className;
        [Newtonsoft.Json.JsonIgnore]
        SysDiagDebug d = new SysDiagDebug();
        public int Faq_id { get; set; }
        public int Q_score { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public string Keyword1 { get; set; }
        public string Keyword2 { get; set; }

        public List<string> keywords;
        
        public FAQ_model()
        {
            keywords = new List<string>();
            Q_score = 0;
            //Keywords = new List<string>();
            className = this.GetType().Name;
        }
        public FAQ_model(string setClassName)
        {
            keywords = new List<string>();
            Q_score = 0;
            //Keywords = new List<string>();
            className = setClassName;
        }
        public List<FAQ_model> GetAllFAQ()
        {
            List<FAQ_model> faqs = new List<FAQ_model>();
            faqs = ravendb.QueryAll<FAQ_model>();
            faqs = faqs.OrderBy(x => x.Faq_id).ToList();
            return faqs;
        }
        public void SaveNew()
        {
            Faq_id = getNewPrimaryKey();
            ravendb.SaveDocument(className+"/"+Faq_id, this);
        }
        public void SaveNewUsingAura(List<string> getKeywords)
        {
            keywords = getKeywords;
            Faq_id = getNewPrimaryKey();
            ravendb.SaveDocument(className + "/" + Faq_id, this);
        }
        public void UpdateFAQ(int id, string[] fields)
        {
            ravendb.UpdateDocument<FAQ_model>(className + "/" + id, fields, this);
        }
        public void UpdateFAQ()
        {
            string[] fields = { "Q_score", "Question", "Answer", "Keyword1", "Keyword2","keywords" };
            ravendb.UpdateDocument<FAQ_model>(className + "/" + this.Faq_id, fields, this);
        }
        public void UpdateFAQUsingAura(List<string> getKeywords)
        {
            string[] fields = { "Q_score", "Question", "Answer", "Keyword1", "Keyword2", "keywords" };
            keywords = getKeywords;
            ravendb.UpdateDocument<FAQ_model>(className + "/" + this.Faq_id, fields, this);
        }
        public void UpdateFAQscore()
        {
            string[] fields = { "Q_score"};
            ravendb.UpdateDocument<FAQ_model>(className + "/" + this.Faq_id, fields, this);
        }
        private int getNewPrimaryKey()
        {
            bool infinite = true;
            FAQ_model faq = new FAQ_model();
            int i = 0;
            while (infinite)
            {
                if(!(GetFAQ(i) is null)) //if not null
                {
                    faq = GetFAQ(i);
                    i++;
                }
                else
                {
                    return i; //if no more records
                }
            }
            return 0;//never reachable
        }
        public FAQ_model GetFAQ(int id)
        {
            return ravendb.ReadDocument<FAQ_model>(className+"/" + id);
        }
        public string GetAnswerById(int id)
        {
            List<FAQ_model> s = new List<FAQ_model>();
            s = ravendb.QueryWhere<FAQ_model>(x => x.Faq_id == id);
            return s[0].Answer;
        }
        public List<string> GetQuestions()
        {
            List<FAQ_model> faqs = new List<FAQ_model>();
            faqs = this.GetAllFAQ();
            faqs.OrderBy(i => i.Faq_id);
            return faqs.Select(FAQ_model => FAQ_model.Question).ToList();
        }
        public List<string> GetAnswers()
        {
            List<FAQ_model> faqs = new List<FAQ_model>();
            faqs = this.GetAllFAQ();
            faqs.OrderBy(i => i.Faq_id);
            return faqs.Select(FAQ_model => FAQ_model.Answer).ToList();
        }
        public List<List<string>> GetKeywords()
        {
            List<FAQ_model> faqs = new List<FAQ_model>();
            faqs = this.GetAllFAQ();
            faqs.OrderBy(i => i.Faq_id);
            List<List<string>> k = new List<List<string>>();
            foreach(FAQ_model faq in faqs)
            {
                k.Add(faq.keywords);
            }
            return k;
        }
        public List<int> GetFAQIds()
        {
            List<FAQ_model> faqs = new List<FAQ_model>();
            faqs = this.GetAllFAQ();
            faqs.OrderBy(i => i.Faq_id);
            return faqs.Select(FAQ_model => FAQ_model.Faq_id).ToList();
        }
        public string GetAnswerFromID(List<string> keywords)
        {
            return null;
        }
        public void DeleteFAQ(string id)
        {
            ravendb.DeleteDocument(className + "/" + id);
        }
        public T GetFAQ<T>(int id)
        {
            return ravendb.ReadDocument<T>(className + "/" + id);
        }
        //for csv
        public override string ToString()
        {
            String newLine = "";
            if (Faq_id > 0)
            {
                newLine = "\n";
            }
            return newLine + "FAQ_id," + (Faq_id + 1) + ",Question," + Question + ",Answer," + Answer;
        }
    }
}
