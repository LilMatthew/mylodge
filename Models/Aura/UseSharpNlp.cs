﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyLodge.proj1.Models.Aura
{
    public class UseSharpNlp
    {
        private string mModelPath = @"I:\C#Projects\modelReleases\";//current model path
        private OpenNLP.Tools.SentenceDetect.MaximumEntropySentenceDetector mSentenceDetector;
        private OpenNLP.Tools.Tokenize.EnglishMaximumEntropyTokenizer mTokenizer;
        private OpenNLP.Tools.PosTagger.EnglishMaximumEntropyPosTagger mPosTagger;
        private OpenNLP.Tools.Chunker.EnglishTreebankChunker mChunker;
        private OpenNLP.Tools.NameFind.EnglishNameFinder mEnNameFinder;
        public static string[] NERmodels = { "date", "location", "money", "organization", "percentage", "person", "time" }; //available models
        public string[] SplitSentences(string paragraph)
        {
            if (mSentenceDetector == null)
            {
                mSentenceDetector = new OpenNLP.Tools.SentenceDetect.EnglishMaximumEntropySentenceDetector(mModelPath + "EnglishSD.nbin");
            }

            return mSentenceDetector.SentenceDetect(paragraph);
        }
        //breaks a text into words, symbols or other meaningful elements
        private string[] TokenizeSentence(string sentence)
        {
            if (mTokenizer == null)
            {
                mTokenizer = new OpenNLP.Tools.Tokenize.EnglishMaximumEntropyTokenizer(mModelPath + "EnglishTok.nbin");
            }

            return mTokenizer.Tokenize(sentence);
        }
        //assigns a part of speech (noun, verb etc.) to each token in a sentence
        private string[] PosTagTokens(string[] tokens)
        {
            if (mPosTagger == null)
            {
                mPosTagger = new OpenNLP.Tools.PosTagger.EnglishMaximumEntropyPosTagger(mModelPath + "EnglishPOS.nbin", mModelPath + @"\Parser\tagdict");
            }

            return mPosTagger.Tag(tokens);
        }
        public string Chunker(string sentence)
        {
            if (mChunker == null)
            {
                mChunker = new OpenNLP.Tools.Chunker.EnglishTreebankChunker(mModelPath + "EnglishChunk.nbin");
            }
            return mChunker.GetChunks(sentence);
        }
        public string NERTag(string sentence)
        {
            if (mEnNameFinder == null)
            {
                mEnNameFinder = new OpenNLP.Tools.NameFind.EnglishNameFinder(mModelPath);
            }
            return mEnNameFinder.GetNames(UseSharpNlp.NERmodels, sentence);
        }
        public UseSharpNlp()
        {
            //constructor
        }
        public UseSharpNlp(string modelPath)
        {
            this.mModelPath = modelPath;
        }
        public string[] GetTokens(string sentence)
        {
            return TokenizeSentence(sentence);
        }
        public string[] GetTags(string sentence)
        {
            return PosTagTokens(GetTokens(sentence));
        }
        public List<string> GetSentenceNouns(string[] sent, string[] tagForWord)
        {
            //Console.WriteLine("running noun splitter!");
            int pos = 0;
            List<string> nouns = new List<string>();
            foreach (String word in sent)
            {
                if (tagForWord[pos] is "NN" || tagForWord[pos] is "NNS")
                {
                    nouns.Add(word);
                    //Console.WriteLine("noun -> " + word);
                }
                pos++;
            }

            return nouns;
        }
        public List<string> GetSentenceVerbs(string[] sent, string[] tagForWord)
        {
            int pos = 0;
            List<string> verbs = new List<string>();
            foreach (String word in sent)
            {
                if (tagForWord[pos] is "VBD" || tagForWord[pos] is "VRB" || tagForWord[pos] is "VB")
                {
                    verbs.Add(word);
                    // Console.WriteLine("verb -> " + word);
                }
                pos++;
            }
            return verbs;
        }
        public List<string> GetSentenceCardinalNumbers(string[] sent, string[] tagForWord)
        {
            int pos = 0;
            List<string> number = new List<string>();
            foreach (String word in sent)
            {
                if (tagForWord[pos] is "VBD" || tagForWord[pos] is "VRB" || tagForWord[pos] is "VB")
                {
                    number.Add(word);
                    // Console.WriteLine("verb -> " + word);
                }
                pos++;
            }
            return number;
        }

    }
}
