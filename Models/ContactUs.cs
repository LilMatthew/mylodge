﻿using MyLodge.proj1.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Raven.Client.Documents;
using Raven.Client.Documents.Session;
using System.ComponentModel.DataAnnotations;
namespace MyLodge.proj1.Models
{
    public class ContactUs
    {
        [Newtonsoft.Json.JsonIgnore]
        Utils.RavenDBConnector raven = Utils.RavenDBConnector.Instance();
        [Newtonsoft.Json.JsonIgnore]
        private string className;
        public int Contactid { get; set; }
        [Required]
        public string Name { get; set; }
       
        public string Email { get; set; }
        public string Message { get; set; }

        public ContactUs()
        {
            this.className = this.GetType().Name;
        }
        public ContactUs(string className)
        {
            this.className = className;
        }
        public ContactUs(int Contactid, string Name, string Email, string Message) : this(Name)
        {
            this.Contactid = Contactid;
            this.Name = Name;
            this.Message = Message;
            this.Email = Email;
           
        }
        public int GetContactid()
        {
            return Contactid;
        }
        public string GetName()
        {
            return Name;
        }

        public void Save()
        {
            this.Contactid = GetNewPK();
            raven.SaveDocument(this.GetType().Name + "/" + Contactid, this);
        }
        public ContactUs Read(int i)
        {
            return raven.ReadDocument<ContactUs>(className + "/" + i);
        }
        public int GetNewPK()
        {
            ContactUs obj = new ContactUs();
            int i = 0;
            try
            {
                bool infinite = true;
                while (infinite)
                {

                    if (!(Read(i) is null))
                    {
                        obj = raven.ReadDocument<ContactUs>(this.GetType().Name + "/" + i);
                        i++;
                    }
                    else
                    {
                        return i;
                        //break;
                    }
                }
            }
            catch (NullReferenceException)
            {

            }
            return i;
        }


        public List<ContactUs> GetAllContactus()
        {

            List<ContactUs> listmember = new List<ContactUs>();
            ContactUs obj = new ContactUs();
            bool infinite = true;
            int i = 0;
            while (infinite)
            {

                if (!(Read(i) is null))
                {
                    obj = raven.ReadDocument<ContactUs>(className + "/" + i);

                    listmember.Add(obj);
                    i++;
                }
                else
                {
                    return listmember;
                }
            }
            return listmember;
        }
       
        public ContactUs GetCustomerByCusid(int cusid)
        {
            List<ContactUs> c = new List<ContactUs>();
            c = raven.QueryWhere<ContactUs>(x => x.Contactid == cusid).ToList();
            try
            {
                if (c[0] is null)
                {
                    return null;
                }
            }
            catch (IndexOutOfRangeException)
            {
                return null;
            }
            catch (ArgumentOutOfRangeException)
            {
                return null;
            }
            return c[0];
        }

      
      
        //GetAllMemberNic
        public List<string> GetCName()
        {
            List<ContactUs> hr = new List<ContactUs>();
            hr = this.GetAllContactus();

            return hr.Select(c => c.Name).ToList();
        }

        //GetAllMemberCountry
        public List<string> GetCEmail()
        {
            List<ContactUs> hr = new List<ContactUs>();
            hr = this.GetAllContactus();

            return hr.Select(c => c.Email).ToList();
        }

        //GetAllMemberAddress
        public List<string> GetMessage()
        {
            List<ContactUs> hr = new List<ContactUs>();
            hr = this.GetAllContactus();

            return hr.Select(c => c.Message).ToList();
        }
        

        public void Delete(int i)
        {
            raven.DeleteDocument(className + "/" + i);
        }


        //add
    }
}
