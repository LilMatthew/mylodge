﻿using MyLodge.proj1.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Raven.Client.Documents;
using Raven.Client.Documents.Session;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using FastMember;
using SodiumSecurity;

namespace MyLodge.proj1.Models
{
    public class Customer
    {
        [Newtonsoft.Json.JsonIgnore]
        Utils.RavenDBConnector raven = Utils.RavenDBConnector.Instance();
        [Newtonsoft.Json.JsonIgnore]
        private string className;

        [Newtonsoft.Json.JsonIgnore]
        private SysDiagDebug d = new SysDiagDebug();
        [Newtonsoft.Json.JsonIgnore]
        Argon2IDUsingLibsodium security = new Argon2IDUsingLibsodium();
        public int Cusid { get; set; }

        [Required]
        public string Fullname { get; set; }
        public DateTime DateofBirth { get; set; }
        [Required]
        public string ContactNo { get; set; }
        public string Email_id { get; set; }
        [Required]
        public string Nic { get; set; }
        public string Country { get; set; }
        public string Address { get; set; }
        [Required]
        public string UserID { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 5)]
        [DataType(DataType.Password)]
        [Newtonsoft.Json.JsonIgnore]
        public string Password { get; set; }

        public byte[] passwordHash;
        public byte[] passwordSalt;

        public Customer()
        {
            this.className = this.GetType().Name;
        }
        public Customer(string className)
        {
            this.className = className;
        }
        public Customer(int Cusid, string fullname, DateTime dateofBirth, string contactNo, string email_id, string nic, string country, string address, string userid, string password) : this(fullname)
        {
            this.Cusid = Cusid;
            this.Fullname = fullname;
            this.ContactNo = contactNo;
            this.Email_id = email_id;
            this.DateofBirth = dateofBirth;
            this.Nic = nic;
            this.Country = country;
            this.Address = address;
            this.UserID = UserID;
            this.Password = password;
        }
        //Argon2ID
        public void SetPassword(string password)
        {
            this.passwordSalt = security.GetGeneratedSalt(password);
            SetHash();
        }
        public void SetHash()
        {
            this.passwordHash = security.GetGeneratedHashBytes(Password, passwordSalt);
        }
        public Boolean PasswordIsMatching(string password)
        {
            byte[] newH = security.GetGeneratedHashBytes(password, this.passwordSalt);
            if (newH.SequenceEqual(passwordHash))
            {
                return true;
            }
            return false;
        }
        //End of Argon2ID

        //for csv        
        public override string ToString()
        {
            String newLine = "";
            if (Cusid > 0)
            {
                newLine = "\n";
            }
            return newLine  + Cusid + "," + Fullname + "," + ContactNo + "," + Email_id + "," + DateofBirth + "," + Nic + "," + Country + "," + UserID;
        }  

        public int GetCusid()
        {
            return Cusid;
        }
       
     
        public void Save()
        {
            this.Cusid = GetNewPK();
            SetPassword(this.Password);
            raven.SaveDocument(this.GetType().Name + "/" + Cusid, this);
        }
        public Customer Read(int i)
        {
            return raven.ReadDocument<Customer>(className + "/" + i);
        }
        public int GetNewPK()
        {
            Customer obj = new Customer();
            int i = 0;
            try
            {
                bool infinite = true;
                while (infinite)
                {

                    if (!(Read(i) is null))
                    {
                        obj = raven.ReadDocument<Customer>(this.GetType().Name + "/" + i);
                        i++;
                    }
                    else
                    {
                        return i;
                        //break;
                    }
                }
            }
            catch (NullReferenceException)
            {

            }
            return i;
        } 

        //update
        public void UpdateCustomer()
        {
            String[] variables = { "Fullname", "ContactNo", "Email_id", "DateofBirth", "Nic", "Country","Address", "UserID", "passwordHash","passwordSalt" };
            //SysDiagDebug d = new SysDiagDebug("Customer>>");
            SetPassword(Password);
            raven.UpdateDocument<Customer>(className + "/" +this.Cusid,variables,this);
        }
        public void ResetPassword(string newPassword)
        {
            String[] variables = { "passwordHash", "passwordSalt" };
            SetPassword(newPassword);
            raven.UpdateDocument<Customer>(className + "/" + this.Cusid, variables, this);
        }
        


       
        public List<Customer> GetAllMemberdetails()
        {

           

            return raven.QueryAll<Customer>();

        }
       
        public Customer GetCustomerByUserId(string userid)
        {
            List<Customer> c = new List<Customer>();
            c = raven.QueryWhere<Customer>(x => x.UserID == userid).ToList();
            try
            {
                if (c[0] is null)
                {
                    return null;
                }
            }
            catch (IndexOutOfRangeException)
            {
                return null;
            }
            catch (ArgumentOutOfRangeException)
            {
                return null;
            }
            return c[0];
        }

       


        public Customer GetCustomerByCusid(int cusid)
        {
            List<Customer> c = new List<Customer>();
            c = raven.QueryWhere<Customer>(x => x.Cusid == cusid).ToList();
            try
            {
                if (c[0] is null)
                {
                    return null;
                }
            }
            catch (IndexOutOfRangeException)
            {
                return null;
            }
            catch (ArgumentOutOfRangeException)
            {
                return null;
            }
            return c[0];
        }

        public Customer GetCustomerByEmail(string email)
        {
            List<Customer> c = new List<Customer>();
            c = raven.QueryWhere<Customer>(x => x.Email_id == email).ToList();
            try
            {
                if (c[0] is null)
                {
                    return null;
                }
            }
            catch (IndexOutOfRangeException)
            {
                return null;
            }
            catch (ArgumentOutOfRangeException)
            {
                return null;
            }
            return c[0];
        }


        //GetAllMemberNames
        public List<string> GetmemberReNames()
        {
            List<Customer> hr = new List<Customer>();
            hr = this.GetAllMemberdetails();

            return hr.Select(Customer => Customer.Fullname).ToList();
        }
        //GetAllMemberDOB
        public List<DateTime> GetmemberReDate()
        {
            List<Customer> hr = new List<Customer>();
            hr = this.GetAllMemberdetails();

            return hr.Select(Customer => Customer.DateofBirth).ToList();
        }

        //GetAllMemberUserID
        public List<string> GetmemberReUserID()
        {
            List<Customer> hr = new List<Customer>();
            hr = this.GetAllMemberdetails();

            return hr.Select(Customer => Customer.UserID).ToList();
        }

        //GetAllMemberEmail_id
        public List<string> GetmemberReEmail_id()
        {
            List<Customer> hr = new List<Customer>();
            hr = this.GetAllMemberdetails();

            return hr.Select(Customer => Customer.Email_id).ToList();
        }

        //GetAllMemberNic
        public List<string> GetmemberReNic()
        {
            List<Customer> hr = new List<Customer>();
            hr = this.GetAllMemberdetails();

            return hr.Select(Customer => Customer.Nic).ToList();
        }

        //GetAllMemberCountry
        public List<string> GetmemberReCountry()
        {
            List<Customer> hr = new List<Customer>();
            hr = this.GetAllMemberdetails();

            return hr.Select(Customer => Customer.Country).ToList();
        }

        //GetAllMemberAddress
        public List<string> GetmemberReAddress()
        {
            List<Customer> hr = new List<Customer>();
            hr = this.GetAllMemberdetails();

            return hr.Select(Customer => Customer.Address).ToList();
        }
        //GetAllMemberContactNo
        public List<string> GetmemberReContactNo()
        {
            List<Customer> hr = new List<Customer>();
            hr = this.GetAllMemberdetails();

            return hr.Select(Customer => Customer.ContactNo).ToList();
        }

        //GetAllMemberPasswords
        public List<string> GetmemberRePassword()
        {
            List<Customer> hr = new List<Customer>();
            hr = this.GetAllMemberdetails();

            return hr.Select(Customer => Customer.Password).ToList();
        }

     
        public Customer Read()
        {
            return raven.ReadDocument<Customer>(className+ "/"+this.Cusid);
        }


        public void Delete(int i)
    {
        raven.DeleteDocument(className + "/" + i);
    }


        public T SetAlternateCast<T>(T obj,int j)
        {
            var fieldNames = typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                            .Select(field => field.Name)
                            .ToList();
            dynamic thisClass = new Customer();
            Customer ar = new Customer();
            ar = ar.Read(j);
            thisClass = ObjectAccessor.Create(ar);
            //thisClass = thisClass.Read();

            for (int i = 0; i < fieldNames.Count; i++)
            {
                PropertyInfo Castproperty = obj.GetType().GetProperty(fieldNames[i]);
                //PropertyInfo Fromproperty = ar.GetType().GetProperty(fieldNames[i]);
                //d.Out("property->" + Castproperty.Name + " thisClass[" + i + "] -> " + thisClass[i]);
                Castproperty.SetValue(obj, thisClass[fieldNames[i]], null);
            }
            return obj;
        }


    }
}
   


