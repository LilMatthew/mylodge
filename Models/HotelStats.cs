﻿using MyLodge.proj1.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyLodge.proj1.Models
{
    public class HotelStats
    {
        [Newtonsoft.Json.JsonIgnore]
        RavenDBConnector raven = RavenDBConnector.Instance();
        public enum Room_Type{S1,S123,S3}

        public string[] room_types = {"standard","s2","s3"};
        public string[] room_id = { "A", "B", "C" };
        public int[] room_type_max = { 10, 5, 3 };
        public int[] room_type_use = { 1, 2, 3 };
        public HotelStats()
        {

        }
        public int GetRoomsTotalFor(Room_Type m)
        {
            if(m == Room_Type.S1)
            {
                return room_type_max[0];
            }else if(m == Room_Type.S123)
            {
                return room_type_max[1];
            }
            else if (m == Room_Type.S3)
            {
                return room_type_max[3];
            }
            return -1;
        }
        public int GetRoomsInUse(Room_Type m)
        {
            if (m == Room_Type.S1)
            {
                return room_type_use[0];
            }
            else if (m == Room_Type.S123)
            {
                return room_type_use[1];
            }
            else if (m == Room_Type.S3)
            {
                return room_type_use[3];
            }
            return -1;
        }

    }
}
