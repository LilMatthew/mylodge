﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace SodiumSecurity
{
    class Argon2IDUsingLibsodium : ISecurity
    {
        private class Na //Na means Sodium
        {
            private const string Name = "libsodium";
            public const int crypto_pwhash_argon2id_ALG_ARGON2ID13 = 2;
            public const long crypto_pwhash_argon2id_OPSLIMIT_SENSITIVE = 4;
            public const int crypto_pwhash_argon2id_MEMLIMIT_SENSITIVE = 1073741824;

            static Na()
            {
                sodium_init();
            }

            [DllImport(Name, CallingConvention = CallingConvention.Cdecl)]
            internal static extern void sodium_init();

            [DllImport(Name, CallingConvention = CallingConvention.Cdecl)]
            internal static extern void randombytes_buf(byte[] buffer, int size);

            [DllImport(Name, CallingConvention = CallingConvention.Cdecl)]
            internal static extern int crypto_pwhash(byte[] buffer, long bufferLen, byte[] password, long passwordLen, byte[] salt, long opsLimit, int memLimit, int alg);
        }
        public byte[] GetGeneratedHashBytes(string password, byte[] salt)
        {
            var hash = new byte[16];
            var result = Na.crypto_pwhash(
                hash,
                hash.Length,
                Encoding.UTF8.GetBytes(password),
                password.Length,
                salt,
                Na.crypto_pwhash_argon2id_OPSLIMIT_SENSITIVE,
                Na.crypto_pwhash_argon2id_MEMLIMIT_SENSITIVE,
                Na.crypto_pwhash_argon2id_ALG_ARGON2ID13
                );

            if (result != 0)
                throw new Exception("An unexpected error has occurred.");

            return hash;
        }

        public byte[] GetGeneratedSalt(string password)
        {
            var buffer = new byte[16];
            Na.randombytes_buf(buffer, buffer.Length);
            return buffer;
        }
        public string GetByte2HexString(byte[] b)
        {
            return BitConverter.ToString(b).Replace("-", ""); 
        }
    }
}
