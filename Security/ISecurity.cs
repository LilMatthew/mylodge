﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SodiumSecurity
{
    interface ISecurity
    { 
        byte[] GetGeneratedSalt(string password);
        byte[] GetGeneratedHashBytes(string password, byte[] salt);
        string GetByte2HexString(byte[] b);
    }
}
