﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MyLodge.proj1.Utils
{
    public class INIFileReader
    {
        public static Dictionary<string,string> ReadINIFile(string path)
        {
            var data = new Dictionary<string, string>();
            foreach (var row in File.ReadAllLines(path))
                data.Add(row.Split('=')[0], string.Join("=", row.Split('=').Skip(1).ToArray()));
            return data;
        }
    }
}
