﻿using FastMember;
using Newtonsoft.Json.Serialization;
using Raven.Client.Documents;
using Raven.Client.Documents.Linq;
using Raven.Client.Documents.Session;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace  MyLodge.proj1.Utils
{
    class RavenDBConnector
    {
        /*
         * Singleton class -> Double lock
         * =======Functions=============
         * void ResetConfig(string url, string dbname) -> not safe
         * -------------------------------------------------------
         * ===Func: returns class of type T
         * T ReadDocument<T>(string id) :returns object of Type T
         * -------------------------------------------------------
         * ===Func: returns exceptions or "success"
         * string SaveDocument(string id, Object classData)
         * string UpdateDocument<T>(string id,string[] fields, Object classAsRef)
         * string DeleteDocument(string doc_id)
         * string RenameDocumentId(Object classData, string old_id, string new_id)
         * -------------------------------------------------------
         * Safety: unsafe
         * Exceptions: on
         * -----------UPDATE v1-----------------------------------
         * List<T> QueryFrom(x=>x.id>0)
         * List<T> QueryAll() //will return all records as list from type T
         * 
         */
        private static readonly object syncLock = new object();
        private static RavenDBConnector instance = null;
        public static string dbUrl = "http://127.0.0.1:8090";
        public static string Database = "Hotel_DB";
        private RavenDBConnector()
        {
            //constructor
        }
        public static void ResetConfig(string url, string dbname)
        {
            dbUrl = url;
            Database = dbname;
        }
        public static RavenDBConnector Instance()
        {
            if (instance == null)
            { //Thread safety
                lock (syncLock) //using lock
                {
                    if (instance == null)
                    {
                        instance = new RavenDBConnector();
                    }
                }
            }
            return instance;
        }
        //Saves or creates new
        /*
        public string SaveToRaven(string className, string id, Object classData)
        {
            //unsafe
            using (IDocumentStore store = new DocumentStore
            {
                Urls = new[] {dbUrl}, //server url:port
                Database = this.Database //dbname
            })
            {
                store.Initialize();
                using (IDocumentSession session = store.OpenSession())

                {
                    try
                    {
                        var ActivatedClass = CreateByTypeName(className);
                        ActivatedClass = classData;
                        session.Store(ActivatedClass, id);
                        session.SaveChanges();
                    }
                    catch (InvalidOperationException e)
                    {
                        return e.ToString();
                    }
                }
            }
            return "Save Successful!";
        }
        */
        //Saves or creates new
        public string SaveDocument(string id, Object classData)
        {
            //unsafe
            using (IDocumentStore store = new DocumentStore
            {
                Urls = new[] { dbUrl }, //server url:port
                Database = Database //dbname
            })
            {

                store.Initialize();
                using (IDocumentSession session = store.OpenSession())

                {
                    session.Store(classData, id);
                    session.SaveChanges();
                    return "Save Success!";
                }
            }
        }
        public T ReadDocument<T>(string id)
        {
            //unsafe
            using (IDocumentStore store = new DocumentStore
            {
                Urls = new[] {dbUrl}, //server url:port
                Database = Database //dbname
            })
            {
                store.Initialize();
                using (IDocumentSession session = store.OpenSession())
                {
                    return session.Load<T>(id);
                }
            }
            //return null;
        }
        public string UpdateDocument<T>(string id,string[] fields, Object classAsRef)
        {
            //unsafe
            using (IDocumentStore store = new DocumentStore
            {
                Urls = new[] {dbUrl}, //server url:port
                Database = Database //dbname
            })
            {
                store.Initialize();
                using (IDocumentSession session = store.OpenSession())
                {
                    dynamic doc = ObjectAccessor.Create(session.Load<T>(id));
                    dynamic classObj = ObjectAccessor.Create(classAsRef);
                    for (int i = 0; i < fields.Length; i++)
                    {
                        //Type t1 = classAsRef.GetType().GetProperty(fields[i]).PropertyType;
                        doc[fields[i]] = classObj[fields[i]];
                        //doc[fields[i]] = classObj[fields[i]]; //check?
                    }
                    session.SaveChanges();
                    return "Update Success!";
                }
            }
            //return null;
        }
        public string DeleteDocument(string doc_id)
        {
            //unsafe
            using (IDocumentStore store = new DocumentStore
            {
                Urls = new[] { dbUrl }, //server url:port
                Database = Database //dbname
            })
            {
                store.Initialize();
                using (IDocumentSession session = store.OpenSession())
                {
                    session.Delete(doc_id);
                    session.SaveChanges();
                }
            }
            return "Delete Success!";
        }
        public string RenameDocumentId(Object classData, string old_id, string new_id)
        {
            string e = SaveDocument(new_id, classData);
            e=" & " + DeleteDocument(old_id);
            return e;
        }
        //MS Activator method, give public access
        public static object CreateByTypeName(string typeName)
        {
            // scan for the class type
            var type = (from assembly in AppDomain.CurrentDomain.GetAssemblies()
                        from t in assembly.GetTypes()
                        where t.Name == typeName 
                        select t).FirstOrDefault();

            if (type == null)
                throw new InvalidOperationException("Type not found");

            return Activator.CreateInstance(type);
        }
        public List<T> QueryWhere<T>(Func<T, bool> condition)
        {
            //List<T> result = new List<T>();
            using (IDocumentStore store = new DocumentStore
            {
                Urls = new[] { dbUrl }, //server url:port
                Database = Database //dbname
            })
            {
                store.Initialize();
                using (IDocumentSession session = store.OpenSession())
                {
                    return session.Query<T>().Where(condition).ToList();
                }

            }
            //return result;
        }
        public List<T> QueryAll<T>()
        {
            //List<T> result = new List<T>();
            using (IDocumentStore store = new DocumentStore
            {
                Urls = new[] { dbUrl }, //server url:port
                Database = Database //dbname
            })
            {
                store.Initialize();
                using (IDocumentSession session = store.OpenSession())
                {
                    return session.Query<T>().ToList();
                }
            }
            //return result;
        }
    }
}
