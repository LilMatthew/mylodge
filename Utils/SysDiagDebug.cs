﻿using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyLodge.proj1.Utils
{
    public class SysDiagDebug
    {
        private static Boolean debugForceDisable = false;
        private Boolean debugMode = true;
        private string logAs = "";
        public SysDiagDebug() { }
        public SysDiagDebug(bool turnLogOff)
        {
            if (turnLogOff)
            {
                Disable();
            }
            else
            {
                Enable();
            }
        }
        public SysDiagDebug(string logAs)
        {
            this.logAs = logAs;
        }
        public void Out(string data)
        {
            if (debugMode && !debugForceDisable)
            {
                //Console.WriteLine(data);
                System.Diagnostics.Debug.WriteLine(logAs + data);
            }
        }
        public static void DisableAll()
        {
            debugForceDisable = true;
        }
        public void Disable()
        {
            debugMode = false;
        }
        public void Enable()
        {
            debugMode = true;
        }
        public static void EnableAll()
        {
            debugForceDisable = true;
        }

    }
}
