#pragma checksum "C:\Users\matth\source\repos\mvcNETCoreApps\MyLodge.proj1\Areas\Admin\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "a2a20c730892d317bfa1ecb1f0ab8d07b5cc2b1b"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_Admin_Views_Home_Index), @"mvc.1.0.view", @"/Areas/Admin/Views/Home/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a2a20c730892d317bfa1ecb1f0ab8d07b5cc2b1b", @"/Areas/Admin/Views/Home/Index.cshtml")]
    public class Areas_Admin_Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("<!DOCTYPE html>\r\n<html>\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("head", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "a2a20c730892d317bfa1ecb1f0ab8d07b5cc2b1b2768", async() => {
                WriteLiteral(@"
    <style>
        body {
            margin: 0;
            padding: 0;
            font-family: sans-serif;
            background: #ffffff;
        }

        .form3 {
            width: 520px;
            padding: 10px;
            position: absolute;
            top: 50%;
            left: 48%;
            transform: translate(-50%,-50%);
            background: #ffffff;
            text-align: left;
            color: black;
            border-style: groove;
            margin-bottom: 10%;
            margin-top: 100px;
            margin-right: 20%;
            margin-left: 40px;
        }

            .form3 h1 {
                color: black;
                text-align: center;
            }

        div.font2 {
            font-size: 15px;
            font-family: Arial;
            font-weight: bold;
            color: #5c5c3d;
            align: center:
        }

        .form3 input[type= ""text""], .form3 input[type= ""password""], .form3 select {
          ");
                WriteLiteral(@"  display: block;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            width: 80%;
            height: 39px;
            padding: 6px 10px;
            outline: none;
            border-width: 1px;
            border-style: solid;
            border-radius: 0;
            background: #fff;
            font: 15px/23px 'Raleway', sans-serif;
            color: #404040;
            appearance: normal;
            -moz-appearance: none;
            -webkit-appearance: none;
            margin-left: 10%;
        }

            .form3 input[type= ""text""]:focus, .form3 input[type=""password""]:focus {
                width: 80%;
                border-color: #119ee7;
            }

        .btn {
            font-size: 16px;
            font-weight: bold;
            border: 0;
            color: #fff;
            width: 200px;
            height: 55px;
            border-radius: 4px;
            background: #119ee7;
            text-transform: upper");
                WriteLiteral(@"case;
            cursor: pointer;
            transition: all 0.3s ease-out 0s;
            padding: 14px 40px;
            margin-left: 155px;
        }

        .checkbox {
            margin-left: 160px;
        }

        .button {
            font-size: 16px;
            font-weight: bold;
            border: 0;
            color: black;
            width: 200px;
            height: 55px;
            border-radius: 4px;
            background: none;
            text-transform: uppercase;
            cursor: pointer;
            transition: all 0.3s ease-out 0s;
            padding: 14px 40px;
            margin-left: 240px;
            display: block;
            margin: 20px auto;
            text-align: center;
            border: 2px solid #119ee7;
            outline: none;
        }

            .button:hover {
                background: #119ee7;
            }

        .butt {
            font-size: 16px;
            font-weight: bold;
            border: 0;");
                WriteLiteral(@"
            color: #fff;
            width: 200px;
            height: 55px;
            border-radius: 4px;
            background: #119ee7;
            text-transform: uppercase;
            cursor: pointer;
            transition: all 0.3s ease-out 0s;
            padding: 14px 40px;
            margin-left: 155px;
        }

        div.text2 {
            margin-left: 155px;
        }

        .style-form [type=submit] {
            background-color: forestgreen;
            color: white;
            font-size: 12px;
            padding: 12px 16px;
            border: none;
            cursor: pointer;
        }
    </style>
    <title>Employee Sign in</title>
");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("body", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "a2a20c730892d317bfa1ecb1f0ab8d07b5cc2b1b7620", async() => {
                WriteLiteral("\r\n\r\n    <div class=\"form3\">\r\n\r\n");
#nullable restore
#line 174 "C:\Users\matth\source\repos\mvcNETCoreApps\MyLodge.proj1\Areas\Admin\Views\Home\Index.cshtml"
         using (Html.BeginForm(new { area = "Admin", controller = "Home", action = "Login" }))
        {

#line default
#line hidden
#nullable disable
                WriteLiteral(@"            <h1>Employee Login</h1>
            <!--
            <div class=""font2""> Already have an account with us? Sign in below.<br> Don’t have an account with us? Click on the Register button to sign up</div><br><br><br>
            -->
            <br>
            <span style=""color:orangered;left:10px;"">");
#nullable restore
#line 181 "C:\Users\matth\source\repos\mvcNETCoreApps\MyLodge.proj1\Areas\Admin\Views\Home\Index.cshtml"
                                                Write(ViewBag.error);

#line default
#line hidden
#nullable disable
                WriteLiteral("</span>\r\n            <input type=\"text\" placeholder=\"NIC\" name=\"nic\"");
                BeginWriteAttribute("value", " value=\"", 5308, "\"", 5316, 0);
                EndWriteAttribute();
                WriteLiteral("><br>\r\n            <input type=\"password\" placeholder=\"Password\" name=\"password\"");
                BeginWriteAttribute("value", " value=\"", 5397, "\"", 5405, 0);
                EndWriteAttribute();
                WriteLiteral("><br>\r\n            <label class=\"checkbox\"><input type=\"checkbox\"");
                BeginWriteAttribute("checked", " checked=\"", 5471, "\"", 5481, 0);
                EndWriteAttribute();
                WriteLiteral(" name=\"remember\"><i></i>Keep me logged in</label><br><br>\r\n            <button class=\"butt\" type=\"submit\">Log in</button>\r\n");
#nullable restore
#line 186 "C:\Users\matth\source\repos\mvcNETCoreApps\MyLodge.proj1\Areas\Admin\Views\Home\Index.cshtml"
        }

#line default
#line hidden
#nullable disable
                WriteLiteral(@"        <br><br><hr><br>
        <div class=""text2"">Don't have an account yet?</div>
        <button class=""button"" type=""submit"" name=""submit"" onclick=""SignUp()"">Register</button>
    </div>
    <script>
    function SignUp() {
        var form = document.createElement(""FORM"");
        form.setAttribute(""id"", ""create"");
        form.setAttribute(""name"", ""create"");
        form.setAttribute(""method"", ""POST"");
        form.setAttribute(""action"", '");
#nullable restore
#line 197 "C:\Users\matth\source\repos\mvcNETCoreApps\MyLodge.proj1\Areas\Admin\Views\Home\Index.cshtml"
                                Write(Url.Action("SignUp", "Home", new { area = "Admin" }));

#line default
#line hidden
#nullable disable
                WriteLiteral("\');\r\n        document.body.appendChild(form);\r\n        document.create.submit();\r\n    }\r\n    </script>\r\n");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n</html>\r\n\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
